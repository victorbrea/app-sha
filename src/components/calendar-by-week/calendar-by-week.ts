import { Component, ViewChild, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Slides, NavController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';
import { UserData } from '../../providers/user-data';


import moment from 'moment';

const NUM_OF_DAYS = 7;

@Component({
  selector: 'calendar-by-week',
  templateUrl: 'calendar-by-week.html'
})
export class CalendarByWeekComponent implements OnInit{
  @ViewChild('mySlider') slider: Slides;
  @Output() daySelected = new EventEmitter();
  @Input() check_in: any;
  @Input() check_out: any;
  public weekNames:Array<String>;
  public selectedDate:any;
  public today:any;
  public weeks:Array<any> = [];
  private numWeeksPrevToCurrent: number;
  public activeMonth: any;
  public slideOptions:any;
  public drag: boolean;
  public checkIn: any;
  public checkOut: any;
 

  constructor (public userData: UserData, private navCtrl: NavController, private translateservice: TranslateService) {
    this.translateservice.get('AGENDA.DIAS-SEMANA').subscribe((val) => {
      this.weekNames = val[this.userData.user.weekStartDay];
    });
    this.today = moment().locale(this.userData.user.lang);
    this.selectedDate = this.today;
    this.numWeeksPrevToCurrent = 0;
    this.drag = false;
  }

  ngOnInit () {
    this.checkIn = moment(this.check_in, 'DD-MM-YYYY').locale(this.userData.user.lang);
    this.checkOut = moment(this.check_out, 'DD-MM-YYYY').locale(this.userData.user.lang);
    let currentWeek;
    if (this.checkOut.isAfter(this.today))
      currentWeek = moment().locale(this.userData.user.lang);
    else
      currentWeek = this.checkIn.clone();
    this.getPreviousWeeks(currentWeek);
    this.init(currentWeek);
    this.getNextWeeks(currentWeek);
    this.slideOptions = {
      initialSlide: this.numWeeksPrevToCurrent
    };
    this.initSelectedDate();
  }

  ionViewDidLoad () {
    
  }

  setTimeToZero (dateLocal) {
    return dateLocal.day(this.userData.user.weekStartDay).hour(0).minute(0).second(0).millisecond(0);
  }

  createWeek (weekObj,forDateObj) {
    weekObj.days = [];
    let count = 0;

    while(count < NUM_OF_DAYS) {
      weekObj.days.push(forDateObj);
      forDateObj = forDateObj.clone();
      forDateObj.add(1, 'd');
      count++;
    }
  }

  initWeek (weekObj) {
    weekObj.selectedDate = weekObj.selectedWeek.clone();
    weekObj.current = weekObj.selectedDate.clone();
    let startWeek = weekObj.selectedDate.clone();
    startWeek.startOf('week');
    this.setTimeToZero(startWeek.day(0));
    this.createWeek(weekObj,startWeek);
  }

  init (week){
    let weekObj = {};
    weekObj['selectedWeek'] = week;
    this.initWeek(weekObj);
    this.weeks.push(weekObj);
  }

  //Crea tantas semanas anteriores a la actual, como semanas haya hasta la fecha del checkIn.
  getPreviousWeeks (currentWeek) {
    let prevWeek = currentWeek.clone().week(currentWeek.week() - 1);
    let endDayOfWeek = prevWeek.clone().endOf('week');
    let done = true;
    while(done) {
      if (this.checkIn.isSameOrBefore(endDayOfWeek)) {
        this.numWeeksPrevToCurrent += 1;
        //**** Mismo código que la función init (week) pero usando unshift en vez de push
          let weekObj = {};
          weekObj['selectedWeek'] = prevWeek;
          this.initWeek(weekObj);
          this.weeks.unshift(weekObj);
        //****
        prevWeek = prevWeek.clone().week(prevWeek.week() - 1);
        endDayOfWeek = endDayOfWeek.clone().subtract(1, 'week');
      } else {
        done = false;
      }
    }
  }

  //Crea tantas semanas posteriores a la actual, como semanas haya hasta la fecha del checkOut.
  getNextWeeks (currentWeek) {
    let nextWeek = currentWeek.clone().week(currentWeek.week() + 1);
    let startDayOfWeek = nextWeek.clone().startOf('week');
    let done = true;
    while(done) {
      if (this.checkOut.isSameOrAfter(startDayOfWeek)) {
        this.init(nextWeek);
        nextWeek = nextWeek.clone().week(nextWeek.week() + 1);
        startDayOfWeek = startDayOfWeek.clone().add(1, 'week');
      } else {
        done = false;
      }
    }
  }

  select (weekObj,day) {
    weekObj.selectedDate = day;
    this.selectedDate = day;
    this.daySelected.next(this.selectedDate.clone().format('DD/MM/YYYY'));
  }

  //Establece un 'selectedDate' por defecto, según el día actual y el perido de estancia
  initSelectedDate () {
    let isBetween = (this.today.isBetween(this.checkIn - 1, this.checkOut + 1))
    let date = (isBetween) ? this.today : this.checkIn;
    let slideIndex = this.numWeeksPrevToCurrent;
    this.activeMonth = this.weeks[slideIndex].selectedWeek.clone().format("MMMM YYYY");
    this.select(this.weeks[slideIndex] , date);
  }

  goToToday () {
    let slideIndex = this.numWeeksPrevToCurrent;
    this.activeMonth = this.weeks[slideIndex].selectedWeek.clone().format("MMMM YYYY");
    this.select(this.weeks[slideIndex] , this.today);
    this.slider.slideTo(slideIndex, 600, false);
  }

  onSlideChanged (swiper) {
    this.drag = false;
    let activeIndex = swiper.activeIndex;
    this.activeMonth = this.weeks[activeIndex].selectedWeek.clone().format("MMMM YYYY");
  }

  onSlideDrag (event) {
    this.drag = true;
  }

}

