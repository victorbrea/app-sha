import { Component, Input } from '@angular/core';
import { UserData } from '../../providers/user-data';

import moment from 'moment';

@Component({
  selector: 'slider-resumen-estancia',
  templateUrl: 'slider-resumen-estancia.html'
})
export class SliderResumenEstanciaComponent {

  @Input() stay: any;
  slideOptions: any;
  moment: any;

  constructor(public userData: UserData) {
    this.slideOptions = {
      autoplay: 7000,
      effect: 'fade',
      pager: false,
      paginationType: 'bullets',
      initialSlide: 0,
      loop: true,
      speed: 3000
    }
    this.moment = moment;  
  }

}
