import { Component, ViewChild, Input, OnInit } from '@angular/core';
import { Slides, NavController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';
import { CitaPage } from '../../pages/cita/cita';
import { CategoriasTratamientosPage } from '../../pages/categorias-tratamientos/categorias-tratamientos';
import { UserData } from '../../providers/user-data';

import moment from 'moment';

const NUM_OF_DAYS = 7;

@Component({
  selector: 'calendar',
  templateUrl: 'calendar.html'
})
export class CalendarComponent implements OnInit{
  @ViewChild('mySlider') slider: Slides;
  @Input() beginOnDate: string;
  public weekNames:Array<String>;
  public selectedDate:any;
  public today:any;
  public citas:Array<any> = [];
  public months:Array<any> = [];
  public slideOptions:any;
  public checkIn: any;
  public checkOut: any;
 

  constructor(public userData: UserData, private navCtrl: NavController, private translateservice: TranslateService) {
    this.translateservice.get('AGENDA.DIAS-SEMANA').subscribe((val) => {
      this.weekNames = val[this.userData.user.weekStartDay];
    });
    this.today = moment().locale(this.userData.user.lang);
    this.checkIn = moment(this.userData.user.checkIn, 'DD-MM-YYYY').locale(this.userData.user.lang);
    this.checkOut = moment(this.userData.user.checkOut, 'DD-MM-YYYY').locale(this.userData.user.lang);

  }

  ngOnInit() {
    this.slideOptions = {
        initialSlide:1
    };

    let currentMonth = moment().locale(this.userData.user.lang);
    let prevMonth = currentMonth.clone().month(currentMonth.month() - 1);
    let nextMonth = currentMonth.clone().month(currentMonth.month() + 1);

    //this.selectedDate = moment().locale(this.userData.user.lang);

    this.init(prevMonth);
    this.init(currentMonth);
    this.init(nextMonth);

    this.initSelectedDate();

    this.updateSliderOnInit();
  }

  setTimeToZero(dateLocal) {
    return dateLocal.day(1).hour(0).minute(0).second(0).millisecond(0);
  }

  createWeek(forDateObj) {
    let weekDays = [],count = 0;
    while(count < NUM_OF_DAYS) {
      weekDays.push(forDateObj);
      forDateObj = forDateObj.clone();
      forDateObj.add(1, 'd');
      count++;
    }
    return weekDays;
  }

  createMonth(monthObj,forMonthObj) {
    monthObj.weeks = [];
    let month = forMonthObj.clone(),done=true;

    while(done) {
      monthObj.weeks.push({ days: this.createWeek(month.clone()) });
      month.add(1, 'w');
      if(month.month() !== monthObj.selectedMonth.month()) {
        done=false;
      }
    }
  }

  initPrev(month){
    let monthObj = {};
    monthObj['selectedMonth'] = month;
    this.initMonth(monthObj);
    this.months.unshift(monthObj);
    this.months.pop();
  }

  initMonth(monthObj) {
    monthObj.selectedDate = monthObj.selectedMonth.clone();
    monthObj.current = monthObj.selectedDate.clone();
    let startMonth = monthObj.selectedDate.clone();
    startMonth.date(1);
    this.setTimeToZero(startMonth.day(0));
    this.createMonth(monthObj,startMonth);
  }

  init(month){
    let monthObj = {};
    monthObj['selectedMonth'] = month;
    this.initMonth(monthObj);
    this.months.push(monthObj);
  }

  //hack to initialize the Native Swiper with the slideOptions as Ionic does not handle it in beta.10 release
  updateSliderOnInit() {
    setTimeout(() => {
      let nativeSwiper = this.slider.getSlider();
      if(nativeSwiper){

        nativeSwiper.update();
        //console.log('Active Index On Init:',nativeSwiper.activeIndex);
        if(nativeSwiper.activeIndex === 0) {
          nativeSwiper.slideTo(1, 0, false);
        }
      }
    }); //estaba a 400 por defecto
  }

  select(monthObj,day) {
    monthObj.selectedDate = day;
    this.selectedDate = day;
    this.citas = this.userData.user.appointments.filter((appointment) => {return appointment['day'] === day.format("DD/MM/YYYY")});
  }

  //Establece un 'selectedDate' por defecto, según el día actual y el perido de estancia
  initSelectedDate() {
    let isBetween = (this.today.isBetween(this.checkIn - 1, this.checkOut + 1))
    let date;
    if (this.beginOnDate)
      date = moment(this.beginOnDate, 'DD/MM/YYYY').locale(this.userData.user.lang);
    else {
      date = (isBetween) ? this.today : this.checkIn;
    }
    this.select(this.months[1] , date);
  }

  handleSlideView(swiper) {
    //console.log(swiper)
    let activeIndex = swiper.activeIndex;
    let activeMonth = this.months[activeIndex].selectedMonth;
    //this.selectedDate = activeMonth;
    let nextMonth = activeMonth.clone().month(activeMonth.month() + 1);
    let prevMonth = activeMonth.clone().month(activeMonth.month() - 1);
    if(activeIndex === 0) {
      this.initPrev(prevMonth);
      swiper.slideTo(1, 0, false);
    } else if(activeIndex === (this.months.length - 1)) {
      this.init(nextMonth);
      this.months.shift();
      swiper.slideTo(this.months.length - 2, 0, false);
    }
  }

  onSlideChanged(swiper) {
    this.handleSlideView(swiper);
  }

  addTreatement($event) {
    this.navCtrl.push(CategoriasTratamientosPage);
  }

  citaTapped($event, cita) {
    this.navCtrl.push(CitaPage, {
      cita: cita
    });
  }

}
