import { Component } from '@angular/core';
import { UserData } from '../../providers/user-data';


@Component({
  selector: 'slider-programs',
  templateUrl: 'slider-programs.html'
})
export class SliderProgramsComponent {

  slideOptions: any;

  constructor(public userData: UserData) {
    this.slideOptions = {
      autoplay: 7000,
      effect: 'fade',
      pager: false,
      paginationType: 'bullets',
      initialSlide: 0,
      loop: true,
      speed: 300
    }
  }

}
