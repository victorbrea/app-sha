export const SHA_LOCATION = {
  address: "Carrer del Verderol, 5 · L'Albir · 03581 · Alicante",
  gps: {
    lat: "38.560136", 
    lon: "-0.074164"
  }
}
export const SHA_WEB = {
  index: {
    es: "https://shawellnessclinic.com/",
    en: "https://shawellnessclinic.com/en/",
    ru: "https://shawellnessclinic.com/en/"
  },
  magazine: {
    es: "https://shawellnessclinic.com/es/shamagazine/",
    en: "https://shawellnessclinic.com/en/shamagazine/", 
    ru: "https://shawellnessclinic.com/en/shamagazine/"
  },
  boutique: {
    es: "https://shawellnessclinic.com/es/sha-boutique/",
    en: "https://shawellnessclinic.com/en/sha-boutique/" , 
    ru: "https://shawellnessclinic.com/en/sha-boutique/"
  },
  privileged: {
    es: "https://shawellnessclinic.com/es/sha-privileged/",
    en: "https://shawellnessclinic.com/en/sha-privileged/",
    ru: "https://shawellnessclinic.com/en/sha-privileged/"
  },
  foundation: {
    es: "https://shawellnessclinic.com/es/fundacion-sha/",
    en: "https://shawellnessclinic.com/en/sha-foundation/",
    ru: "https://shawellnessclinic.com/en/sha-foundation/"
  },
  esenza: {
    es: "http://esenzabysha.com/es/",
    en: "http://esenzabysha.com/en/",
    ru: "http://esenzabysha.com/en/"
  },
  ab: {
    es: "http://ab-living.com/es/",
    en: "http://ab-living.com/",
    ru: "http://ab-living.com/"
  },
  programas: {
    es: "https://shawellnessclinic.com/es/programas/",
    en: "https://shawellnessclinic.com/en/health-programmes/",
    ru: "https://shawellnessclinic.com/ru/nashy-programmy/"
  },
  tratamientos: {
    es: "https://shawellnessclinic.com/es/area-clinica/",
    en: "https://shawellnessclinic.com/en/clinic-area/",
    ru: "https://shawellnessclinic.com/ru/klinicheskaya-medicina/"
  },
  bookings: {
    es: "https://shawellnessclinic.com/reserva-online/",
    en: "https://shawellnessclinic.com/en/online-reservations/",
    ru: "https://shawellnessclinic.com/en/online-reservations/"
  },
  conditions_shafire_points: {
    es: "https://shawellnessclinic.com/es/sha-privileged/",
    en: "https://shawellnessclinic.com/en/sha-privileged/",
    ru: "https://shawellnessclinic.com/en/sha-privileged/"
  }
}
export const SHA_TELEPHONE = {
  default:   "+34966811199",
  agenda:    "+34966868084",
  contactar: "+34966854085"
}

export const SHA_WHATSAPP = {
  GR: "34664257999",
  GA: "34664257986"
} //OJO! el numero debe contener el código del pais y no debe incluirse el caracter '+'

export const SHA_EMAIL = {
  reservas: "reservations@shawellnessclinic.com",
  ventas: "sales@shawellnessclinic.com",
  comunicacion: "communications@shawellnessclinic.com",
  contactar: "yourconcierge@shawellnessclinic.com"
}

export const SERVER = "https://health.shawellnessclinic.com/api/Connector/Post";
export const SERVER_SATISFACCION_ICONS = "https://health.shawellnessclinic.com/Images/cuestionarioSatisfaccion/icon/";
//export const SERVER = "https://shawellnessclinic.dyndns.org/api/Connector/Post";
export const LANGUAGES = ['es','en'];  //['es','en','ru']
export const LANGUAGE_DEFAULT = 'en';
export const FASES = ['preSha', 'Sha', 'postSha'];

export const URL_API_TIEMPO = "http://www.aemet.es/xml/municipios/localidad_03011.xml"
export const URL_API_IMAGES_TIEMPO = "http://www.aemet.es/es/imagenes/png/estado_cielo/"

export const URL_TRIPADVISOR = "https://www.tripadvisor.es/UserReviewEdit-g790189-d1500401-SHA_Wellness_Clinic-El_Albir_L_Alfas_del_Pi_Costa_Blanca_Province_of_Alicante_Valencian_Country.html"


export const CACHED_IMAGES = true;


//Constantes configuración notificaciones push
export const ONE_SIGNAL_CODE = "0a0a89c2-afcc-4d8e-8367-7bfca316b5bb";
export const PUSH_NOTIFICATIONS_CODE = "529215014337";

//Tratamientos
export interface Treatment {
  id: string,
  duration: any,
  name: string,
  brief: string,
  image: string,
  videoId: string,
  description: string,
  price: string,
  hidden: string   //ocultar en tratamientos adicionales
}

//Citas agenda. Tratamientos agendados o recomendados
export interface Appointment {
  id: string,
  name: string,
  day: string,
  start: string,  // '10:00' formato 24h
  end: string,    // '16:00' formato 24h
  room: string,
  type: string,   // 'included', 'free', 'aditional'
  notScheduled: string,  // no agendable
  doctor: {
    id: string,
    name: string,
    image: string
  },
  duration: string,
  description: string,
  advice: string,
  image: string,
  videoId: string,
  price: string  
}

export interface TreatmentsByCategory {
  id: string,
  name: string,
  description: string,
  image: string,
  treatments: Array<{Treatment}>
}

//disponibilidad de un tratamiento concreto
export interface TreatmentAvailability {
  day: string,
  start: string,
  end: string,
  room: string,
  doctor: {
    id: string,
    name: string,
    image: string
  }
}

export interface User {
  id: string,
  name: string,
  image: string,
  email: string,
  email_medical: string,
  phone:string,
  fase: string,  //valores establecidos en Array FASES.
  programs: Array<{
    name: string,
    image: string
  }>,
  suite: Suite,
  additionalTreatments: Array<string>,
  preferences: {
    shoes: string,
    bathrobe: string,
    mattress: string,
    pillow: string,
    cleaning_period: string,
    coverage_period: string
  },
  careTeam: Array<{
    id: string,
    role: string,
    name: string,
    image: string,
  }>,
  prescriptions: Array<{Prescription}>,
  stays: Array<{Stay}>,
  diet: Array<{DailyDiet}>,
  shafire: {
    points: string,
    category: string,
    description: Array<string>
    footer: Array<string>
  },
  checkIn: string,   // '10/11/2020'
  checkOut: string, 
  appointments: Array<{Appointment}>,
  bookingId: string,
  lang: string,  /**** dato en almacenamiento local ****/
  adviceTime: string, /**** dato en almacenamiento local ****/
  weekStartDay: number  /**** dato en almacenamiento local ****/
                        // 0 comienza en domingo, 1 comienza en lunes
}

export interface Doctor {
  id: string,
  name: string,
  image: string,
  specialities: Array<string>,
  cv: string,
  treatments: Array<{Treatment}>
}

export interface Suite {
  name: string
}

export interface Prescription {
  doctor: {
    id: string,
    name: string,
    image: string
  }
  text: string,
  day: string
}

export interface Stay {
  checkIn: string,
  checkOut: string,
  weightIn: string,
  weightOut: string,
  programs: Array<{
    name: string,
    image: string
  }>,
  diet: Array<{DailyDiet}>,
  treatmentsList: Array<string>,
  prescriptions: Array<{Prescription}>,
  doctorEquipment: Array<{
    id: string,
    name: string, 
    image: string
  }>

}

export interface DailyDiet {
  day: string,
  breakfast: {
    menu: string,
    te: string,
    plates: Array<string>
  },
  lunch: {
    menu: string,
    te: string,
    plates: Array<string>
  },
  te: {
    te1: string,
    te2: string,
    plates: Array<string> 
  },
  dinner:{
    menu: string,
    te: string,
    plates: Array<string>
  }
  recommendations: string
}

export interface Notification {
  id: number,
  title: string,
  text: string,
  at: any,
  every: any,
  data: {
    type: string,
    title: string,
    description: string,
    image: string,
    link: string,
    content: string,
    viewed: boolean,
    lastDayViewed: string,
    params: any
  }
}

export interface SHAAcademy {
  date: string,
  activities: Array<{
    hourIn: string,
    hourOut: string,
    name: string,
    level: string,
    place: string
  }>
}
