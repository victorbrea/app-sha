import { Component, ViewChild, OnInit} from '@angular/core';
import { Nav, Platform, MenuController, Events } from 'ionic-angular';
import { StatusBar, Splashscreen, LocalNotifications, OneSignal } from 'ionic-native';
import { TranslateService } from 'ng2-translate';
import { Globalization } from 'ionic-native';
import { LANGUAGES, LANGUAGE_DEFAULT, SHA_WEB, PUSH_NOTIFICATIONS_CODE, ONE_SIGNAL_CODE } from '../models/models';

import { Subscription } from 'rxjs';

import { UserData } from '../providers/user-data';
import { LocalStorageService } from '../providers/local-storage-service';
import { NotificationsService } from '../providers/notifications-service';

import { LoginPage } from '../pages/login/login';
import { AgendaPage } from '../pages/agenda/agenda';
import { MiPerfilPage } from '../pages/mi-perfil/mi-perfil';
import { MiLlegadaPage } from '../pages/mi-llegada/mi-llegada';
import { NotificacionesPage } from '../pages/notificaciones/notificaciones';
import { NewsletterPage } from '../pages/newsletter/newsletter';
import { EstiloVidaPage } from '../pages/estilo-vida/estilo-vida';
import { ConfiguracionPage } from '../pages/configuracion/configuracion';
import { MiEstanciaPage } from '../pages/mi-estancia/mi-estancia';
import { EstanciasAnterioresPage } from '../pages/estancias-anteriores/estancias-anteriores';
//import { ConfigurarSalidaPage } from '../pages/configurar-salida/configurar-salida';
import { CategoriasTratamientosPage } from '../pages/categorias-tratamientos/categorias-tratamientos';
import { CitaPage } from '../pages/cita/cita';
import { OnBoardingPage } from '../pages/on-boarding/on-boarding';
import { ShaAcademyPage } from '../pages/sha-academy/sha-academy';
import { SinConexionPage } from '../pages/sin-conexion/sin-conexion';
import { CuestionarioSatisfaccionPage } from '../pages/cuestionario-satisfaccion/cuestionario-satisfaccion';


import 'moment/min/locales';


@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit{
  @ViewChild(Nav) nav: Nav;

  notificationRedirectPage: Array<any>;
  rootPage: any;
  activePage: any;
  pages: Array<{title: string, component: any, icon:string}>;
  pagesPreSha: Array<{title: string, component: any, icon:string}>;
  pagesPostSha: Array<{title: string, component: any, icon:string}>;
  pagesSha: Array<{title: string, component: any, icon:string}>;
  badge: number;
  urlImageUser: string;

  onResumeSubscription: Subscription;

  constructor(public platform: Platform, public userData: UserData, private localNotificationService: NotificationsService, private translateService: TranslateService, private localStorage: LocalStorageService, private events: Events, private menuCtrl: MenuController) {

    this.pagesPreSha = [
      { title: 'MENU.MI_LLEGADA', component: MiLlegadaPage, icon: "mi-llegada" },
      { title: 'MENU.AGENDA', component: AgendaPage, icon: "agenda" },
      { title: 'MENU.TRATAMIENTOS', component: CategoriasTratamientosPage, icon: "tratamientos" },
      { title: 'MENU.ESTILO_VIDA', component: EstiloVidaPage, icon: "estilo-de-vida" },
      { title: 'MENU.NOTIFICACIONES', component: NotificacionesPage, icon: "notificaciones" },
      { title: 'MENU.MI_PERFIL', component: MiPerfilPage, icon: "usuario" },
      { title: 'MENU.AJUSTES', component: ConfiguracionPage, icon: "configuracion" }
    ];

    this.pagesSha = [
      { title: 'MENU.MI_ESTANCIA', component: MiEstanciaPage, icon: "mi-estancia" },
      { title: 'MENU.AGENDA', component: AgendaPage, icon: "agenda" },
      { title: 'MENU.NOTIFICACIONES', component: NotificacionesPage, icon: "notificaciones" },
      { title: 'MENU.NEWSLETTER', component: NewsletterPage, icon: "newsletter" },
      { title: 'MENU.TRATAMIENTOS', component: CategoriasTratamientosPage, icon: "tratamientos" },
      { title: 'MENU.SHA_ACADEMY', component: ShaAcademyPage, icon: "sha-academy" },
      { title: 'MENU.ESTILO_VIDA', component: EstiloVidaPage, icon: "estilo-de-vida" },
      { title: 'MENU.MI_PERFIL', component: MiPerfilPage, icon: "usuario" },
      { title: 'MENU.CUESTIONARIO_SATISFACCION', component: CuestionarioSatisfaccionPage, icon: "satisfaccion" },
      { title: 'MENU.AJUSTES', component: ConfiguracionPage, icon: "configuracion" }
    ];

    this.pagesPostSha = [
      { title: 'MENU.ESTANCIAS_ANTERIORES', component: EstanciasAnterioresPage, icon: "mi-llegada" },
      { title: 'MENU.ESTILO_VIDA', component: EstiloVidaPage, icon: "estilo-de-vida" },
      { title: 'MENU.NOTIFICACIONES', component: NotificacionesPage, icon: "notificaciones" },
      { title: 'MENU.TRATAMIENTOS', component: CategoriasTratamientosPage, icon: "tratamientos" },
      { title: 'MENU.MI_PERFIL', component: MiPerfilPage, icon: "usuario" },
      { title: 'MENU.CUESTIONARIO_SATISFACCION', component: CuestionarioSatisfaccionPage, icon: "satisfaccion" },
      { title: 'MENU.AJUSTES', component: ConfiguracionPage, icon: "configuracion" }
    ];

    this.urlImageUser = "";

    this.onResumeSubscription = platform.resume.subscribe(() => {
      this.userData.getFase().subscribe(
        result => {
            if (this.userData.user.fase != result){
              this.listenToEvents();
              this.initializeApp();
            }
        },
        error => {
        }
      )
    });

  }

  ngOnInit() {
    this.platform.ready().then(() => {
      StatusBar.styleDefault();
      Splashscreen.hide();
      this.listenToEvents();
      this.initializeApp();
    });
  }

  ngOnDestroy() {
    // always unsubscribe your subscriptions to prevent leaks
    this.onResumeSubscription.unsubscribe();
  }

//inicialización de la app
  initializeApp() {
    //inicializa los evetos click y trigger de las notificaciones locales
    if(this.platform.is('cordova')) {
      this.eventLocalNotificationsHandles();
    }
    this.initLang().then(()=>{
      let id = this.localStorage.getUserLogged();
      //Evalúa si el usuario se ha logueado previamente y redirige según corresponda
      if (id !== null) {
        if (navigator.onLine) {
          this.userData.load(id).subscribe(
            () => this.events.publish('user:loggedIn', this.userData.user.image),
            (error) => console.log(error)
          );
        } else {
          this.nav.setRoot(SinConexionPage);
        }
      } else {
        this.rootPage = LoginPage;
      }
    })
  }

//Inicializa el lenguaje de la app
  private initLang() {
    return new Promise(resolve=> {
      let lang = this.localStorage.getLang();
      if (LANGUAGES.indexOf(lang) > -1) {
        this.setAppLang(lang);
        resolve()
      } else if(this.platform.is('cordova')) {
        
        Globalization.getPreferredLanguage().then((lg) => {
          lang = lg.value.split('-', 1)[0].toLowerCase();
          this.setAppLang(lang);
          this.userData.setLang(lang)
          resolve();
        });
        
        this.setAppLang(LANGUAGE_DEFAULT);
        this.userData.setLang(LANGUAGE_DEFAULT);
        resolve()
      } else {
        this.setAppLang(LANGUAGE_DEFAULT);
        this.userData.setLang(LANGUAGE_DEFAULT);
        resolve()
      }
    })
  }

  private setAppLang(lang: string) {
    this.userData.user.lang = lang;
    //Establece el lenguaje de la aplicación
    this.translateService.setDefaultLang(lang);
    this.translateService.use(lang);
  }

  openPage(page) {
    this.nav.setRoot(page);
    this.activePage = page;
  }

  setRootPage() {
    if(!this.notificationRedirectPage) {
      this.nav.setRoot(this.activePage);
    } else {
      this.nav.setPages(this.notificationRedirectPage);
    }
  }

  evaluateRootPage ()  {
    this.menuCtrl.enable(true, 'menu');
    let onBoardingViewed = (this.localStorage.getOnBoarding() == this.userData.user.fase);
    switch (this.userData.user.fase) {
      case "preSha":
        this.pages = this.pagesPreSha;
        this.activePage = this.notificationRedirectPage ? this.notificationRedirectPage[0] : ((onBoardingViewed) ? MiLlegadaPage : OnBoardingPage);
        break;
      case "Sha":
        this.pages = this.pagesSha;
        this.activePage = this.notificationRedirectPage ? this.notificationRedirectPage[0] : ((onBoardingViewed) ? MiEstanciaPage : OnBoardingPage);
        break;
      case "postSha":
        this.pages = this.pagesPostSha;
        this.activePage = this.notificationRedirectPage ? this.notificationRedirectPage[0] : ((onBoardingViewed) ? EstanciasAnterioresPage : OnBoardingPage);
        break;
    }
    this.setRootPage();
  }

  checkPageActive (page) {
    return page == this.activePage;
  }

  reservarTapped () {
    this.platform.ready().then(() => {
      window.open(SHA_WEB.bookings[this.userData.user.lang],'_blank', 'location=yes,toolbar=yes')
    });
  }


  listenToEvents() {

/*
    //Rellena el primer elemento del menu con el nombre del usuario
    this.events.subscribe('user:name', (userEventData) => {
      this.pages[0].title = userEventData[0];
    })

    //Refresca la imagen de usuario en el menu
    this.events.subscribe('user:image', (userEventData) => {
      this.urlImageUser = userEventData[0];
    })
*/
    //Si el usuario está logueado
    this.events.subscribe('user:loggedIn', (userImage) => {
      this.urlImageUser = userImage;
      this.evaluateRootPage();
      //inicializa las notificaciones locales y push
      if(this.platform.is('cordova')) {
        this.initializatePushNotifications()
        this.localNotificationService.scheduleAllNotifications();
      }
      this.badge = this.localNotificationService.badge || 0;
    });

    //Establece highlight en el menu si se redirecciona a la agenda desde otro link que no sea el menu
    this.events.subscribe('menu:agenda', () => {
      this.activePage = AgendaPage;
    });

    //Establece highlight en el menu si se redirecciona a la newsletter desde otro link que no sea el menu
    this.events.subscribe('menu:newsletter', () => {
      this.activePage = NewsletterPage;
    });

    //Establece highlight en el menu si se redirecciona a la Mi LLegada desde otro link que no sea el menu
    this.events.subscribe('menu:miLlegada', () => {
      this.activePage = MiLlegadaPage;
    });

    //Establece highlight en el menu si se redirecciona a la Mi Estancia desde otro link que no sea el menu
    this.events.subscribe('menu:miEstancia', () => {
      this.activePage = MiEstanciaPage;
    });

    //Establece highlight en el menu si se redirecciona a la Mi Estancia desde otro link que no sea el menu
    this.events.subscribe('menu:estiloDeVida', () => {
      this.activePage = EstiloVidaPage;
    });

    //Establece highlight en el menu si se redirecciona a EstanciasAnterioresPage desde otro link que no sea el menu
    this.events.subscribe('menu:estanciasAnterioresPage', () => {
      this.activePage = EstanciasAnterioresPage;
    });

    //Actualiza el badge de la pestaña notificaciones
    this.events.subscribe('menu:badge', () => {
      this.badge = this.localNotificationService.badge;
    });

    //Inicializa la app cuando vuelve la conexion a internet. El evento se lanza desde 'SinConexionPage'
    this.events.subscribe('initializeApp', () => {
      this.initializeApp();
    });

  }


  initializatePushNotifications() {
    OneSignal.startInit(ONE_SIGNAL_CODE, PUSH_NOTIFICATIONS_CODE);

    OneSignal.inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification);

    OneSignal.handleNotificationReceived().subscribe((jsonData) => {
      let notificationType = jsonData.payload.additionalData.type;
      this.localNotificationService.getNotificationTemplate(notificationType).then(notification => {
        this.localNotificationService.showNotification(notification);
      });
    });

    OneSignal.handleNotificationOpened().subscribe((jsonData) => {
      let notificationType = jsonData.notification.payload.additionalData.type
      this.localNotificationService.getNotificationTemplate(notificationType).then(notification => {
        this.localNotificationService.showNotification(notification);
        this.localNotificationService.markViewedNotificationsToShow(notification['id']);
        switch (notificationType) {
          case "agenda":
            this.notificationRedirectPage = [AgendaPage];
            break;
          case "newsletter":
            this.notificationRedirectPage = [NewsletterPage];
            break;
          default:
            this.initializeApp();
            break;
        }
        if(this.nav.getActive())
          this.evaluateRootPage();
      });
    });

    OneSignal.setSubscription(true);

    OneSignal.sendTag("fase", this.userData.user.fase);

    OneSignal.syncHashedEmail(this.userData.user.email);

    OneSignal.endInit();
  }


  eventLocalNotificationsHandles () {
    LocalNotifications.on("click", (notifElemen, state) => {
      let notification = notifElemen;
      notification.data = JSON.parse(notification.data);
      if(notification.data.type == "citaReminder") {
        this.notificationRedirectPage = [AgendaPage, {page: CitaPage, params: {cita: notification.data.params}}];
        //this.localNotificationService.hideNotification(notification);
      }
/*      else if(notification.data.type == "configurarSalida") {
        this.notificationRedirectPage = [MiEstanciaPage, {page: ConfigurarSalidaPage, params: {form: {}}}];
      }
*/
      else
        this.notificationRedirectPage = [NotificacionesPage];
      //Comprueba si hay alguna página activa.
      if(this.nav.getActive())
        this.evaluateRootPage()
    });
    this.localNotificationService.eventNotificationsHandles();
  }

}
