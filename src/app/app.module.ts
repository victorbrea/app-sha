import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, enableProdMode } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Http, HttpModule } from '@angular/http';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';

import { UserData } from '../providers/user-data';
import { LoadingService } from '../providers/loading-service';
import { LocalStorageService } from '../providers/local-storage-service';
import { RemoteStorageService } from '../providers/remote-storage-service';
import { NotificationsService } from '../providers/notifications-service';
import { WeatherLocationService } from '../providers/weather-location-service';
import { TreatmentsData } from '../providers/treatments-data';
import { DoctorData } from '../providers/doctor-data';
import { ShaAcademyData } from '../providers/sha-academy-data';
import { NewsletterData } from '../providers/newsletter-data';

import { To12hour } from '../pipes/to12hour';

import { CalendarComponent } from '../components/calendar/calendar';
import { CalendarByWeekComponent } from '../components/calendar-by-week/calendar-by-week';
import { SliderProgramsComponent } from '../components/slider-programs/slider-programs';
import { SliderResumenEstanciaComponent } from '../components/slider-resumen-estancia/slider-resumen-estancia';

import { AgendaPage } from '../pages/agenda/agenda';
import { ComentarPage } from '../pages/comentar/comentar';
import { MiLlegadaPage } from '../pages/mi-llegada/mi-llegada';
import { NotificacionesPage } from '../pages/notificaciones/notificaciones';
import { FichaConsejoPage } from '../pages/ficha-consejo/ficha-consejo';
import { EstiloVidaPage } from '../pages/estilo-vida/estilo-vida';
import { ConfiguracionPage } from '../pages/configuracion/configuracion';
import { MiPerfilPage } from '../pages/mi-perfil/mi-perfil';
import { MiEstanciaPage } from '../pages/mi-estancia/mi-estancia';
import { TratamientosPage } from '../pages/tratamientos/tratamientos';
import { CategoriasTratamientosPage } from '../pages/categorias-tratamientos/categorias-tratamientos';
import { CitaPage } from '../pages/cita/cita';
import { ConfigurarLlegadaPage } from '../pages/configurar-llegada/configurar-llegada';
import { LoginPage } from '../pages/login/login';
import { RecuperarPasswordPage } from '../pages/recuperar-password/recuperar-password';
import { CuestionarioSaludPage } from '../pages/cuestionario-salud/cuestionario-salud';
import { FichaTratamientoPage } from '../pages/ficha-tratamiento/ficha-tratamiento';
import { PoliticaPrivacidadSaludPage } from '../pages/politica-privacidad-salud/politica-privacidad-salud';
import { VideoPage } from '../pages/video/video';
import { NewsletterPage } from '../pages/newsletter/newsletter';
import { FichaTerapeutaPage } from '../pages/ficha-terapeuta/ficha-terapeuta';
import { OnBoardingPage } from '../pages/on-boarding/on-boarding';
import { ShaAcademyPage } from '../pages/sha-academy/sha-academy';
import { FichaPrescripcionesPage } from '../pages/ficha-prescripciones/ficha-prescripciones';
import { EstanciasAnterioresPage } from '../pages/estancias-anteriores/estancias-anteriores';
import { FichaResumenEstanciaPage } from '../pages/ficha-resumen-estancia/ficha-resumen-estancia';
import { TratamientosIncluidosPage } from '../pages/tratamientos-incluidos/tratamientos-incluidos';
import { DietaPage } from '../pages/dieta/dieta';
import { TelefonosInteresPage } from '../pages/telefonos-interes/telefonos-interes';
import { InstalacionesHorariosPage } from '../pages/instalaciones-horarios/instalaciones-horarios';
import { ConfigurarSalidaPage } from '../pages/configurar-salida/configurar-salida';
import { CambiarContrasenaPage } from '../pages/cambiar-contrasena/cambiar-contrasena';
import { FichaCategoriaShafirePage } from '../pages/ficha-categoria-shafire/ficha-categoria-shafire';
import { SinConexionPage } from '../pages/sin-conexion/sin-conexion';
import { FichaDetallesProgramaPage } from '../pages/ficha-detalles-programa/ficha-detalles-programa';
import { FichaDetallesAcademyPage } from '../pages/ficha-detalles-academy/ficha-detalles-academy';
import { FichaDetallesNewsletterPage } from '../pages/ficha-detalles-newsletter/ficha-detalles-newsletter';
import { SolicitarFacturaPage } from '../pages/solicitar-factura/solicitar-factura';
import { FichaInstalacionHorariosPage } from '../pages/ficha-instalacion-horarios/ficha-instalacion-horarios';
import { CondicionesUsoPage } from '../pages/condiciones-uso/condiciones-uso';
import { CuestionarioSatisfaccionPage } from '../pages/cuestionario-satisfaccion/cuestionario-satisfaccion';


export function createTranslateLoader(http: Http) {
    return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

enableProdMode();

@NgModule({
  declarations: [
    MyApp,
    AgendaPage,
    CalendarComponent,
    CalendarByWeekComponent,
    SliderProgramsComponent,
    SliderResumenEstanciaComponent,
    ComentarPage,
    MiLlegadaPage,
    NotificacionesPage,
    FichaConsejoPage,
    EstiloVidaPage,
    ConfiguracionPage,
    MiPerfilPage,
    LoginPage,
    MiEstanciaPage,
    TratamientosPage,
    CategoriasTratamientosPage,
    CitaPage,
    ConfigurarLlegadaPage,
    RecuperarPasswordPage,
    CuestionarioSaludPage,
    FichaTratamientoPage,
    VideoPage,
    PoliticaPrivacidadSaludPage,
    NewsletterPage,
    FichaTerapeutaPage,
    OnBoardingPage,
    ShaAcademyPage,
    FichaPrescripcionesPage,
    EstanciasAnterioresPage,
    FichaResumenEstanciaPage,
    TratamientosIncluidosPage,
    DietaPage,
    TelefonosInteresPage,
    InstalacionesHorariosPage,
    ConfigurarSalidaPage,
    CambiarContrasenaPage,
    FichaCategoriaShafirePage,
    SinConexionPage,
    FichaDetallesProgramaPage,
    SolicitarFacturaPage,
    FichaInstalacionHorariosPage,
    FichaDetallesAcademyPage,
    FichaDetallesNewsletterPage,
    CondicionesUsoPage,
    CuestionarioSatisfaccionPage,
    To12hour
  ],
  imports: [
    BrowserModule,
    HttpModule,
    TranslateModule.forRoot({
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http] 
    }),
    IonicModule.forRoot(MyApp,{
        platforms: {
            ios: {
                backButtonText: ''
            }
        }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AgendaPage,
    ComentarPage,
    MiLlegadaPage,
    NotificacionesPage,
    FichaConsejoPage,
    EstiloVidaPage,
    ConfiguracionPage,
    MiPerfilPage,
    LoginPage,
    MiEstanciaPage,
    TratamientosPage,
    CategoriasTratamientosPage,
    ConfigurarLlegadaPage,
    CitaPage,
    RecuperarPasswordPage,
    CuestionarioSaludPage,
    FichaTratamientoPage,
    VideoPage,
    PoliticaPrivacidadSaludPage,
    FichaTerapeutaPage,
    NewsletterPage,
    OnBoardingPage,
    ShaAcademyPage,
    FichaPrescripcionesPage,
    EstanciasAnterioresPage,
    FichaResumenEstanciaPage,
    TratamientosIncluidosPage,
    DietaPage,
    TelefonosInteresPage,
    ConfigurarSalidaPage,
    InstalacionesHorariosPage,
    CambiarContrasenaPage,
    FichaCategoriaShafirePage,
    SinConexionPage,
    FichaDetallesProgramaPage,
    SolicitarFacturaPage,
    FichaInstalacionHorariosPage,
    FichaDetallesNewsletterPage,
    FichaDetallesAcademyPage,
    CuestionarioSatisfaccionPage,
    CondicionesUsoPage
  ],
  providers: [UserData, TreatmentsData, DoctorData, ShaAcademyData, NewsletterData, LoadingService, LocalStorageService, RemoteStorageService, NotificationsService, WeatherLocationService, {provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
