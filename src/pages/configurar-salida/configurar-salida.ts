import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, ActionSheetController, Platform } from 'ionic-angular';
import { CallNumber } from 'ionic-native';
import { TranslateService } from 'ng2-translate';
import { UserData } from '../../providers/user-data';
import { LoadingService } from '../../providers/loading-service';
import { NotificationsService } from '../../providers/notifications-service';
import { SHA_TELEPHONE, SHA_EMAIL, SHA_WHATSAPP } from '../../models/models'

import moment from 'moment';


@Component({
  selector: 'page-configurar-salida',
  templateUrl: 'configurar-salida.html'
})
export class ConfigurarSalidaPage {

  form: any;
  configured: boolean;
  transferConfiguredState: boolean;
  invoiceConfiguredState: boolean;
  sendButtonText: string;
  transferPrice: any;
  checkOut: any;

  constructor(public navCtrl: NavController, private navParams: NavParams, private platform: Platform, private actionsheetCtrl: ActionSheetController, private translateService: TranslateService, private alertCtrl: AlertController, public modalCtrl: ModalController, public userData: UserData, private loading: LoadingService, private notifications: NotificationsService) {
    let data = this.navParams.get('form');
    this.checkOut = moment(this.userData.user.checkOut, 'DD/MM/YYYY').locale(this.userData.user.lang);
    if (data)
      this.form = data;
    //convierte 0 1 a true
    this.form.checkOut = this.checkOut.format('DD/MM/YYYY');
    this.form.transfer = this.form.transfer ? true : false;
    this.form.invoice = this.form.invoice ? true : false;
    this.configured = this.form.configured ? true : false;
    this.transferConfiguredState = this.form.transfer;
    this.invoiceConfiguredState = this.form.invoice;
    this.transferPrice = (this.form.transferPrice && this.form.transferPrice != 0) ? this.form.transferPrice + " €" : "€";
  }

  ionViewDidLoad() {}

  openActionSheet () {
    this.translateService.get('ACTION_SHEET_CONTACT').subscribe((val) => {
      let actionSheet = this.actionsheetCtrl.create({
        title: val.TITLE,
        cssClass: 'action-sheet',
        buttons: [
          {
            text: val.LLAMAR,
            icon: !this.platform.is('ios') ? 'call' : null,
            handler: () => {
              let tlfNumber = SHA_TELEPHONE.contactar || undefined;
              if(tlfNumber) {
                CallNumber.callNumber(tlfNumber, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch(() => window.open("tel:" + tlfNumber, '_system'));
              }
            }
          },
          {
            text: val.WHATSAPP,
            icon: !this.platform.is('ios') ? 'logo-whatsapp' : null,
            handler: () => {
              let tlfNumber = SHA_WHATSAPP.GR || undefined;
              if(tlfNumber) {
                window.open("whatsapp://send?phone=" + tlfNumber, '_system');
              }
            }
          },
          {
            text: val.ENVIAR_EMAIL,
            icon: !this.platform.is('ios') ? 'mail-open' : null,
            handler: () => {
              let email = SHA_EMAIL.reservas || undefined;
              if(email)
                window.open("mailto:" + email, '_system');
            }
          },
          {
            text: val.CANCELAR,
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel', // will always sort to be on the bottom
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    })
  }

  submitForm() {
    this.loading.show();
    //convierte true false a 0 1
    this.form.transfer = this.form.transfer ? 1 : 0;
    this.form.invoice = this.form.invoice ? 1 : 0;
    this.form.name = this.userData.user.name;
    console.log(this.form);
    this.userData.sendDeparture(this.form).subscribe(
      (msg) => {
        this.navCtrl.pop();
        this.loading.showToast(msg);
      },
      (error) => this.loading.showAlert(error)
    );
  }


  tappedSubmit() {
    this.translateService.get('CONF_LLEGADA').subscribe((val) => {

      if(this.form.transfer){
        this.alertCtrl.create({
          title: val.CONFIRM_TITLE,
          message: val.CONFIRM_MSG,
          buttons: [
            {
              text: val.CANCELAR,
              role: 'cancel',
              handler: () => {}
            },
            {
              text: val.ACEPTAR,
              handler: () => {
                this.submitForm();
              }
            }
          ]
        }).present();
      } else {
        this.submitForm();
      }

    });
  }


  tappedInvoice ($event) {
    if(this.configured)
      $event.checked = this.invoiceConfiguredState;
  }


  tappedTransfer ($event) {
    if(this.configured) {
      $event.checked = this.transferConfiguredState;
    }
    else {
      if(!$event.checked) {
        this.transferPrice = "€";
        this.form.place = "";
        this.form.comment = "";
        this.form.carType = "";
        this.form.people = "";
      }
    }
  }

  changedTransferPlace(place) {
    //console.log(place);
    this.userData.getTransferPrice(this.form).subscribe(
        (value) => {
          this.loading.hide();
          this.transferPrice = (value == 0) ? '€' : value + ' €';
          this.form.transferPrice = value;
        },
        (error) => this.loading.showAlert(error)
      );
  }

  changeDate() {
    let hourMin = this.form.hourOut.split(':');
    if((hourMin[0] > 12) || (hourMin[0] == 12) && (hourMin[1] > 0)) {
      this.translateService.get('CONF_SALIDA').subscribe((val) => {
        this.alertCtrl.create({
          title: val.ATENCION,
          message: val.ATENCION_MESSAGE,
          buttons: [
            {
              text: val.ACEPTAR,
              handler: () => {}
            }
          ]
        }).present();
      });
    }
  }

}

