import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, AlertController, ActionSheetController, Platform } from 'ionic-angular';
import { CallNumber } from 'ionic-native';
import { TranslateService } from 'ng2-translate';
import { UserData } from '../../providers/user-data';
import { LoadingService } from '../../providers/loading-service';
import { NotificationsService } from '../../providers/notifications-service';
import { SHA_TELEPHONE, SHA_EMAIL, SHA_WHATSAPP } from '../../models/models'

import moment from 'moment';


@Component({
  selector: 'page-configurar-llegada',
  templateUrl: 'configurar-llegada.html'
})
export class ConfigurarLlegadaPage {

  form: any;
  configured: boolean;
  parkingConfiguredState: boolean;
  transferConfiguredState: boolean;
  sendButtonText: string;
  checkIn: any;
  checkInNext: any;
  transferPrice: any;

  constructor(public navCtrl: NavController, private navParams: NavParams, private platform: Platform, private actionsheetCtrl: ActionSheetController, private translateService: TranslateService, private alertCtrl: AlertController, public modalCtrl: ModalController, public userData: UserData, private loading: LoadingService, private notifications: NotificationsService) {
    this.checkIn = moment(this.userData.user.checkIn, 'DD/MM/YYYY').locale(this.userData.user.lang);
    this.checkInNext = this.checkIn.clone().add(1, 'days');
    this.form = this.navParams.get('form');
    this.form.checkIn = this.checkIn.format('DD/MM/YYYY');
    //convierte 0 1 a true
    this.form.parking = this.form.parking ? true : false;
    this.form.transfer = this.form.transfer ? true : false;
    this.configured = this.form.configured ? true : false;
    this.parkingConfiguredState = this.form.parking;
    this.transferConfiguredState = this.form.transfer;
    this.transferPrice = (this.form.transferPrice && this.form.transferPrice != 0) ? this.form.transferPrice + " €" : "€";
  }

  ionViewDidLoad() {}

  openActionSheet () {
    this.translateService.get('ACTION_SHEET_CONTACT').subscribe((val) => {
      let actionSheet = this.actionsheetCtrl.create({
        title: val.TITLE,
        cssClass: 'action-sheet',
        buttons: [
          {
            text: val.LLAMAR,
            icon: !this.platform.is('ios') ? 'call' : null,
            handler: () => {
              let tlfNumber = SHA_TELEPHONE || undefined;
              if(tlfNumber) {
                CallNumber.callNumber(tlfNumber.default, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch(() => window.open("tel:" + tlfNumber, '_system'));
              }
            }
          },
          {
            text: val.ENVIAR_EMAIL,
            icon: !this.platform.is('ios') ? 'mail-open' : null,
            handler: () => {
              let email = SHA_EMAIL.reservas || undefined;
              if(email)
                window.open("mailto:" + email, '_system');
            }
          },
          {
            text: val.CANCELAR,
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel', // will always sort to be on the bottom
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    })
  }

  submitForm() {
    this.loading.show();
    //convierte true false a 0 1
    this.form.parking = this.form.parking ? 1 : 0;
    this.form.transfer = this.form.transfer ? 1 : 0;
    this.form.name = this.userData.user.name;
    console.log(this.form);
    this.userData.sendArrival(this.form).subscribe(
      (msg) => {
        //this.notifications.scheduleConfirmMiLlegadaNotification(this.form.checkIn, this.form.hourIn);
        this.loading.showToast(msg);
        this.navCtrl.pop();
      },
      (error) => this.loading.showAlert(error)
    );
  }

  tappedSubmit() {
    this.translateService.get('CONF_LLEGADA').subscribe((val) => {
      if(this.form.transfer){
        this.alertCtrl.create({
          title: val.CONFIRM_TITLE,
          message: val.CONFIRM_MSG,
          buttons: [
            {
              text: val.CANCELAR,
              role: 'cancel',
              handler: () => {}
            },
            {
              text: val.ACEPTAR,
              handler: () => {
                this.submitForm();
              }
            }
          ]
        }).present();
      } else {
        this.submitForm();
      }
    });
  }

  tappedTransfer ($event) {
    if(this.configured) {
      $event.checked = this.transferConfiguredState;
    }
    else {
    /*  if(!$event.checked) {
        this.transferPrice = "€";
        this.form.place = "";
        this.form.code = "";
        this.form.hourTransfer = "";
        this.form.comment = "";
        this.form.carType = "";
        this.form.people = "";
      } else {
        this.form.hourIn = "";
      }
    */
    }
  }

  tappedParking ($event) {
    if(this.configured)
      $event.checked = this.parkingConfiguredState;
  }

  changedTransferPlace(place) {
    console.log(place);
    this.form.place = place;
    if(place == 'otro') {
      this.transferPrice = "€";
      this.translateService.get('CONF_LLEGADA').subscribe((val) => {
        this.alertCtrl.create({
          title: val.CONTACTAR_TITLE,
          message: val.CONTACTAR_MSG,
          buttons: [
            {
              text: val.CANCELAR,
              role: 'cancel',
              handler: () => {}
            },
            {
              text: val.CONTACTAR,
              handler: () => {
                this.openActionSheet();
              }
            }
          ]
        }).present();
      });      
    } else {
      this.getTransferPrice();
    }
  }

  getTransferPrice() {
    console.log(this.form);
    this.userData.getTransferPrice(this.form).subscribe(
      (value) => {
        this.loading.hide();
        this.transferPrice = (value == 0) ? '€' : value + ' €';
        this.form.transferPrice = value;
      },
      (error) => this.loading.showAlert(error)
    );    
  }

}
