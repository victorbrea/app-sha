import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CallNumber} from 'ionic-native';
import { UserData } from '../../providers/user-data';
import { Http } from '@angular/http';
import { FichaInstalacionHorariosPage } from '../ficha-instalacion-horarios/ficha-instalacion-horarios';
import 'rxjs/add/operator/map';


@Component({
  selector: 'page-instalaciones-horarios',
  templateUrl: 'instalaciones-horarios.html'
})
export class InstalacionesHorariosPage {

  items: any;

  constructor(public navCtrl: NavController, private http: Http, private userData: UserData) {
    if(!this.items) {
      this.http.get('assets/i18n/facilitiesSchedules.json').map(res => res.json())
        .subscribe(data => {
          this.items = (data[this.userData.user.lang].length == 0) ? data['es'] : data[this.userData.user.lang];
        }
      );
    }
  }

  ionViewDidLoad() {}

  telephoneTapped(tlf_number) {
    CallNumber.callNumber(tlf_number, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => window.open("tel:" + tlf_number, '_system'));
  }

  itemTapped(item) {
      this.navCtrl.push(FichaInstalacionHorariosPage, {item: item});
  }

}
