import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

import { LoadingService } from '../../providers/loading-service';
import { UserData } from '../../providers/user-data';

/*
  Generated class for the RecuperarPassword page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-recuperar-password',
  templateUrl: 'recuperar-password.html'
})
export class RecuperarPasswordPage {

  email: string;

  constructor(public navCtrl: NavController, private loading: LoadingService, public translateService: TranslateService, private userData: UserData) {
    this.email = "";
  }

  ionViewDidLoad() {}

  submitForm() {
    this.loading.show();
    this.userData.recoveryPassword(this.email).subscribe(
      (msgObj) => {
        this.loading.showAlert(msgObj['subtitle'], msgObj['title']);
        this.navCtrl.pop();
      },
      (error) => {
        this.loading.showAlert(error)
      }
    );
  }

}
