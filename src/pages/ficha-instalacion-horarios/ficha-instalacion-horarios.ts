import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-ficha-instalacion-horarios',
  templateUrl: 'ficha-instalacion-horarios.html'
})
export class FichaInstalacionHorariosPage {

  item: any;
  image: any;

  constructor(public navCtrl: NavController, private navParams: NavParams) {
      this.item = this.navParams.get('item');
      this.image = this.item.image;
      this.image = this.image.split(".jpg")[0] + "-full.jpg";
  }

}
