import { Component } from '@angular/core';
import { NavController, ActionSheetController, Platform, AlertController } from 'ionic-angular';
import { LaunchNavigator, LaunchNavigatorOptions, CallNumber } from 'ionic-native';
import { UserData } from '../../providers/user-data';
import { TranslateService } from 'ng2-translate';
import { ConfigurarLlegadaPage } from '../../pages/configurar-llegada/configurar-llegada';
import { CuestionarioSaludPage } from '../../pages/cuestionario-salud/cuestionario-salud';
import { FichaDetallesProgramaPage } from '../../pages/ficha-detalles-programa/ficha-detalles-programa';
import { AgendaPage } from '../agenda/agenda';
import { InstalacionesHorariosPage } from '../instalaciones-horarios/instalaciones-horarios';
import { LoadingService } from '../../providers/loading-service';
import { SHA_EMAIL, SHA_LOCATION, SHA_TELEPHONE, SHA_WHATSAPP } from '../../models/models';

import moment from 'moment';



@Component({
  selector: 'page-mi-llegada',
  templateUrl: 'mi-llegada.html'
})
export class MiLlegadaPage {

  location: any;
  checkIn: any;
  checkOut: any;
  numNights: number;
  buttonArrivalConfig: any = {
    title: null,
    subtitle: null
  }
  formMyArrivalConfig: any;
  showSuite: any = false;
  showPrograms: any = false;
  additionalTreatments: any = [];
  showAdditionalTreatments: any = false;
  isSentCuestionario: any = true;
  isTwoDaysBeforeCheckIn: any;

  constructor(private platform: Platform, private navCtrl: NavController, private alertCtrl: AlertController, private translateService: TranslateService, private loading: LoadingService, private actionsheetCtrl: ActionSheetController ,public userData: UserData) {
    this.location = SHA_LOCATION;
    this.checkIn = moment(this.userData.user.checkIn, 'DD-MM-YYYY').locale(this.userData.user.lang);
    this.checkOut = moment(this.userData.user.checkOut, 'DD-MM-YYYY').locale(this.userData.user.lang);
    this.numNights = this.checkOut.diff(this.checkIn, 'days');
    this.formMyArrivalConfig = false;
    this.additionalTreatments = this.compressArray(this.userData.user.additionalTreatments);
  }

  ionViewDidLoad() {}

  ionViewWillEnter() {
    this.userData.isSentCuestionarioSalud().subscribe(
      res => {this.isSentCuestionario = res; this.getMyArrivalState()},
      err => this.getMyArrivalState()
    );
  }

  getMyArrivalState() {
    this.userData.getArrival().subscribe(
      (form) => {
        console.log(form);
        if(form) {
          this.formMyArrivalConfig = form;
          let twoDaysBefore = this.checkIn.clone().subtract(1, 'days');
          let now = moment();
          this.isTwoDaysBeforeCheckIn = now.isSameOrBefore(twoDaysBefore);
          if (form['configured']) {
            this.translateService.get('MI_LLEGADA').subscribe((val) => {
              this.buttonArrivalConfig.title = val.MIS_DATOS;
              this.buttonArrivalConfig.subtitle = val.CONSULTA_HORA;
            });
          } else {
            this.translateService.get('MI_LLEGADA').subscribe((val) => {
              this.buttonArrivalConfig.title = val.CONFIGURAR_LLEGADA_BUTTON;
              this.buttonArrivalConfig.subtitle = val.CONFIGURAR_LLEGADA_SUBTITLE;
            });
          }
        }
      },
      (error) => {}
    );
  }


  openMapsApp() {
    let destination = this.location.gps.lat + "," + this.location.gps.lon;
    let origin = "";
    let options: LaunchNavigatorOptions = {
      start: origin
    }

    LaunchNavigator.navigate(destination, options)
      .then(
        success => console.log('Launched navigator'),
        error => {
          console.log('Error launching navigator: ' + error)
          //window.open("http://maps.google.com/?q=" + destination, '_system');
        }
    );
  }

  openConfigLlegada() {
/*    if (this.isTwoDaysBeforeCheckIn || this.formMyArrivalConfig['configured']) {
      this.navCtrl.push(ConfigurarLlegadaPage, {form: this.formMyArrivalConfig});
    } else {
      this.translateService.get('MI_LLEGADA').subscribe((val) => {
          this.loading.showAlert(val.NO_CONFIGURAR_LLEGADA);
      });
    }
*/
    this.navCtrl.push(ConfigurarLlegadaPage, {form: this.formMyArrivalConfig});
  }

  openActionSheet () {
    this.translateService.get('ACTION_SHEET_CONTACT').subscribe((val) => {
      let actionSheet = this.actionsheetCtrl.create({
        title: val.TITLE,
        cssClass: 'action-sheet',
        buttons: [
          {
            text: val.LLAMAR,
            icon: !this.platform.is('ios') ? 'call' : null,
            handler: () => {
              let tlfNumber = SHA_TELEPHONE.default || undefined;
              if(tlfNumber) {
                CallNumber.callNumber(tlfNumber, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch(() => window.open("tel:" + tlfNumber, '_system'));
              }
            }
          },
          {
            text: val.ENVIAR_EMAIL,
            icon: !this.platform.is('ios') ? 'mail-open' : null,
            handler: () => {
              let email = SHA_EMAIL.reservas || undefined;
              if(email)
                window.open("mailto:" + email, '_system');
            }
          },
          {
            text: val.CANCELAR,
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel', // will always sort to be on the bottom
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    });
  }

  openProgramDetails(program) {
    this.navCtrl.push(FichaDetallesProgramaPage, {program});
  }

  openCuestionario() {
    if(!this.isSentCuestionario)
      this.navCtrl.push(CuestionarioSaludPage);
    else
     this.openAlert();

  }

  showTapped(type) {
    switch (type) {
      case "programs":
        this.showPrograms = !this.showPrograms;
        break;
      case "additionalTreatments":
        this.showAdditionalTreatments = !this.showAdditionalTreatments;
        break; 
      case "suite":
        this.showSuite = !this.showSuite;
        break;   
      default:
        // code...
        break;
    }
  }

  compressArray(original) {
      let compressed = [];
      // make a copy of the input array
      let copy = original.slice(0);
     
      // first loop goes over every element
      for (let i = 0; i < original.length; i++) {
     
        let myCount = 0;  
        // loop over every element in the copy and see if it's the same
        for (let w = 0; w < copy.length; w++) {
          if (original[i] == copy[w]) {
            // increase amount of times duplicate is found
            myCount++;
            // sets item to undefined
            delete copy[w];
          }
        }
     
        if (myCount > 0) {
          let a: any = [];
          a['value'] = original[i];
          a['count'] = myCount;
          compressed.push(a);
        }
      }
      return compressed;
    };


  additionalTreatmentsTapped() {
    this.showAdditionalTreatments = !this.showAdditionalTreatments;
  }

  agendaTapped () {
    this.navCtrl.push(AgendaPage);
  }

  horariosTapped () {
    this.navCtrl.push(InstalacionesHorariosPage);
  }

  openAlert() {
    this.translateService.get('MI_LLEGADA').subscribe((val) => {
      this.alertCtrl.create({
        message: val.CUESTIONARIO_RECIBIDO_SUBTITLE,
        buttons: [
          {
            text: val.CANCELAR,
            role: 'cancel',
            handler: () => {}
          },
          {
            text: val.MODIFICAR,
            handler: () => {
              this.openActionSheet();
            }
          }
        ]
      }).present();
    });
  }


}