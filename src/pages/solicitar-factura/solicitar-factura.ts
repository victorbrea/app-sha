import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { LoadingService } from '../../providers/loading-service';

/*
  Generated class for the SolicitarFacturaPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-solicitar-factura',
  templateUrl: 'solicitar-factura.html'
})
export class SolicitarFacturaPage {

  form: any = {
    suite: false,
    email: false,
    email_address: null
  }

  constructor(public navCtrl: NavController, public loading: LoadingService, public viewCtrl: ViewController, public userData: UserData) {
    this.form.email_address = this.userData.user.email;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  submit() {
    let params = {
      userId: this.userData.user.id,
      userName: this.userData.user.name,
      bookingId: this.userData.user.bookingId,
      toSuite: this.form.suite,
      toEmail: this.form.email,
      emailAddress: this.form.email_address
    }
    this.loading.show();
    this.userData.requestInvoice(params).subscribe(
      (msg) => {
        this.loading.showToast(msg);
        this.dismiss();
      },
      (error) => this.loading.showAlert(error)
    );
  }

}
