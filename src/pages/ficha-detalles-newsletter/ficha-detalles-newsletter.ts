import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { CACHED_IMAGES } from '../../models/models';

/*
  Generated class for the FichaDetallesProgramaPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-ficha-detalles-newsletter',
  templateUrl: 'ficha-detalles-newsletter.html'
})
export class FichaDetallesNewsletterPage {

  item: any;
  itemSection: any;
  cachedImages = CACHED_IMAGES;

  constructor(public navCtrl: NavController, private platform: Platform, public navParams: NavParams) {
    this.item = this.navParams.get('item');
    this.itemSection = this.navParams.get('section');
  }

  ionViewDidLoad() {}

  openUrl (url) {
    this.platform.ready().then(() => {
      window.open(url,'_blank', 'location=yes,toolbar=yes')
    });
  }

  getRandom() {
    return Date.now();
  }

}
