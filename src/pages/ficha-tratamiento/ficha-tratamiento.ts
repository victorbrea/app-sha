import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, AlertController, Select, Content, NavParams } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';
import { AgendaPage } from '../agenda/agenda';
import { VideoPage } from '../video/video';
import { Treatment, TreatmentAvailability } from '../../models/models';
import { LoadingService } from '../../providers/loading-service';
import { UserData } from '../../providers/user-data';
import { TreatmentsData } from '../../providers/treatments-data';
import { CACHED_IMAGES } from '../../models/models';

import moment from 'moment';


@Component({
  selector: 'page-ficha-tratamiento',
  templateUrl: 'ficha-tratamiento.html'
})
export class FichaTratamientoPage implements OnInit{
  @ViewChild(Content) content: Content;
  @ViewChild("selectDay") selectDay: Select;
  @ViewChild("selectHour") selectHour: Select;


  treatment: Treatment;
  showTitle: boolean = false;
  form: any;
  arrayAvailability: Array<TreatmentAvailability>;
  arrayDays: any = [];
  arrayHours: any;
  moment: any;
  showDisponibilidad: boolean = false;
  cachedImages = CACHED_IMAGES;

  constructor(private navCtrl: NavController, private userData: UserData, private treatmentsData: TreatmentsData, private alertCtrl: AlertController, private navParams: NavParams, private loading: LoadingService, private translateService: TranslateService) {
    this.arrayHours = [];
    this.form = {
      room: null,
      hour: {}
    }
    this.form.day = this.navParams.get('day') || moment().format('DD/MM/YYYY');
    this.treatment = this.navParams.get('treatment');
    console.log(this.treatment);
    this.arrayAvailability = [];
    this.moment = moment;
  }

  ngOnInit() {}

  ionViewDidLoad() {
    //escucha el evento scroll para ocultar o mostrar el titulo de la pagina
    this.content.addScrollListener(event => {
      let scrollPosition = event.target.scrollTop;
      this.showTitle = (scrollPosition > 268) ? true : false;
    })
  }

  ionViewWillEnter() {
      this.fillArrayDays();
      this.showDisponibilidad = false;
  }

  submitForm() {
    if (this.showDisponibilidad) {
      this.translateService.get('TRATAMIENTO').subscribe((val) => {
        this.alertCtrl.create({
          title: `${val.CONFIRMAR}<p>${this.treatment.name}</p>`,
          message: `<!--<div class="listado"><img src="assets/icon/localizador-blue.png" class="icono-pequeno" alt=""><span>${this.form.room}</span></div>-->
                    <div class="listado"><img src="assets/icon/agenda-blue.png" class="icono-pequeno" alt=""><span>${this.getDayFormated(this.form.day)}</span></div>
                    <div class="listado"><img src="assets/icon/reloj-blue.png" class="icono-pequeno" alt=""><span>${this.form.hour.start} (${this.treatment.duration} ${val.MINUTOS})
                    </span></div>
                    <!--<div class="listado"><img src="assets/icon/doctor-blue.png" class="icono-pequeno" alt=""><span>${this.form.hour.doctor.name}</span></div>-->
                    <div class="listado"><img src="assets/icon/precio-blue.png" class="icono-pequeno" alt=""><span>${this.treatment.price}</span></div>
                    <p>${val.MESSAGE}</p>
                    `,
          cssClass: "custom-alert",
          buttons: [
            {
              text: val.CANCELAR,
              role: 'cancel',
              handler: () => {}
            },
            {
              text: val.ACEPTAR,
              handler: () => this.scheduleTreatment()
            }
          ]
        }).present();
      });
    } else {
      this.translateService.get('TRATAMIENTO').subscribe((val) => {
        this.alertCtrl.create({
          title: val.TITLE,
          message: val.MESSAGE,
          cssClass: "custom-alert",
          buttons: [
            {
              text: val.CANCELAR,
              role: 'cancel',
              handler: () => {
                this.showDisponibilidad = false;
              }
            },
            {
              text: val.ACEPTAR,
              handler: () => {
                  this.showDisponibilidad = true;
                  this.selectDay.open();
              }
            }
          ]
        }).present();
      });
    }
  }

  openVideo() {
    if (navigator.onLine) {
      this.navCtrl.push(VideoPage, {videoId: this.treatment.videoId})
    }
  }

  checkHourIsSelected() {
    console.log(this.form.hour)
  }

  getSelectFormHours() {
    //console.log(this.userData.user.id+ " / " + this.treatment.id+ " / " + this.userData.user.bookingId+ " / " + this.form.day)
    this.translateService.get('LOADING').subscribe((val) => {
      this.loading.show(val.ESPERE2);
    });
    this.treatmentsData.getTreatmentAvailability(this.userData.user.id, this.treatment.id, this.userData.user.bookingId, this.form.day).subscribe(
      result => {
        this.loading.hide();

        for (let i in result) {
          this.arrayAvailability.push(result[i]);
        }
        this.arrayAvailability.sort(function (a, b) {
          if (a.start < b.start) return -1;
          if (a.start > b.start) return 1;
          return 0;
        });

        if(this.arrayAvailability.length != 0) {
          this.form.hour = this.arrayAvailability[0];
          setTimeout(() => {
            this.selectHour.open();
          }, 100);
        } else {
          this.translateService.get('TRATAMIENTO').subscribe((val) => {
            this.alertCtrl.create({
              title: val.SIN_DISPONIBILIDAD_TITULO,
              message: val.SIN_DISPONIBILIDAD_MSG,
              buttons: [
                {
                  text: val.ACEPTAR,
                  handler: () => {
                      this.selectDay.open();
                  }
                }
              ]
            }).present();
          });
        }
      },
      error => this.loading.showAlert(error)
    );

  }

  getDayFormated (day) {
    return moment(day + " 12:00",'DD/MM/YYYY HH:mm').locale(this.userData.user.lang).format('LLLL').split(' 12:00')[0]
  }

  scheduleTreatment() {
    //console.log(this.form.hour)
    this.loading.show();
    this.treatmentsData.scheduleTreatment(this.userData.user.bookingId, this.userData.user.id, this.treatment.id, this.form.day, this.form.hour.start, this.form.hour.end, this.treatment.price, this.form.hour.doctor.id).subscribe(
      result => {
        this.loading.showAlert(result);
        (this.navCtrl.first().component == AgendaPage) ? this.navCtrl.popToRoot() : '';
      },
      error => this.loading.showAlert(error)
    );
  }

  fillArrayDays() {

    let today = moment().format('DD/MM/YYYY')
    let end = this.userData.user.checkOut
    let betweenDays = moment(end,'DD/MM/YYYY').diff(moment(today,'DD/MM/YYYY'), 'days');

    this.arrayDays[0] = today;

    for(let i = 1; i < betweenDays + 1; i++) {
      this.arrayDays[i] = moment(today,'DD/MM/YYYY').add(i, 'days').format('DD/MM/YYYY');
      //console.log(this.arrayDays[i])
    }
  }

  getRandom() {
    return Date.now();
  }

}
