import { Component } from '@angular/core';
import { NavController, ActionSheetController, Platform, ModalController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { AgendaPage } from '../agenda/agenda';
import { DietaPage } from '../dieta/dieta';
import { FichaPrescripcionesPage } from '../ficha-prescripciones/ficha-prescripciones';
import { TelefonosInteresPage } from '../telefonos-interes/telefonos-interes';
import { InstalacionesHorariosPage } from '../instalaciones-horarios/instalaciones-horarios';
import { ConfigurarSalidaPage } from '../configurar-salida/configurar-salida';
import { FichaTerapeutaPage } from '../ficha-terapeuta/ficha-terapeuta';
import { FichaDetallesProgramaPage } from '../ficha-detalles-programa/ficha-detalles-programa';
import { SolicitarFacturaPage } from '../solicitar-factura/solicitar-factura';
import { TranslateService } from 'ng2-translate';
import { CallNumber } from 'ionic-native';
import { NotificationsService } from '../../providers/notifications-service';
import { SHA_EMAIL, SHA_TELEPHONE, SHA_WHATSAPP, CACHED_IMAGES } from '../../models/models';

import moment from 'moment';

@Component({
  selector: 'page-mi-estancia',
  templateUrl: 'mi-estancia.html'
})
export class MiEstanciaPage {

  checkIn: any;
  checkOut: any;
  buttonDepartureConfig: any = {
    title: null,
    subtitle: null
  }
  formMyDepartureConfig: any;
  isTwoDaysBeforeCheckOut: any;
  professionals: any = [];
  showProfessionals: any = false;
  showPrograms: any = false;
  showSuite: any = false;
  showAdditionalTreatments: any = false;
  additionalTreatments: any = [];
  showCareTeam: any = false;
  cachedImages = CACHED_IMAGES;


  constructor(public navCtrl: NavController, public modalCtrl: ModalController, private translateService: TranslateService, private platform: Platform, public userData: UserData, private actionsheetCtrl: ActionSheetController, private notifications: NotificationsService) {
    this.checkIn = moment(this.userData.user.checkIn, 'DD-MM-YYYY').locale(this.userData.user.lang);
    this.checkOut = moment(this.userData.user.checkOut, 'DD-MM-YYYY').locale(this.userData.user.lang);
    this.formMyDepartureConfig = false;
    this.additionalTreatments = this.compressArray(this.userData.user.additionalTreatments);
    //this.professionals = this.getProfessionals();
  }

  ionViewDidLoad() {}

  ionViewDidEnter() {
    this.getMyDepartureState()
  }

  //Extrae los doctores que aparecen en cada una de las citas del array appointments, limpiando los repetidos.
  getProfessionals() {
    return (this.userData.user.appointments.map(appointment => {return appointment['doctor']}).filter((obj, pos, arr) => {return arr.map(mapObj => mapObj['id']).indexOf(obj['id']) === pos}))
  }

  showTapped(type) {
    switch (type) {
      case "careTeam":
        this.showCareTeam = !this.showCareTeam;
        break;  
      case "professionals":
        this.showProfessionals = !this.showProfessionals;
        break;
      case "programs":
        this.showPrograms = !this.showPrograms;
        break;
      case "additionalTreatments":
        this.showAdditionalTreatments = !this.showAdditionalTreatments;
        break; 
      case "suite":
        this.showSuite = !this.showSuite;
        break;   
      default:
        // code...
        break;
    }
  }

  openProgramDetails(program) {
    this.navCtrl.push(FichaDetallesProgramaPage, {program});
  }

  professionalTapped(person) {
    this.navCtrl.push(FichaTerapeutaPage, {id: person.id, role: person.role});
  }

  getMyDepartureState() {
    this.userData.getDeparture().subscribe(
      (form) => {
        if(form) {
          this.formMyDepartureConfig = form;
          let twoDaysBefore = this.checkOut.clone().subtract(2, 'days');
          let now = moment();
          this.isTwoDaysBeforeCheckOut = now.isSameOrAfter(twoDaysBefore);
          if (form['configured']) {
            this.translateService.get('ESTANCIA').subscribe((val) => {
              this.buttonDepartureConfig.title = val.DATOS_SALIDA_TITLE;
              this.buttonDepartureConfig.subtitle = val.DATOS_SALIDA_SUBTITLE;
              this.notifications.cancelConfigurarSalidaNotification();
            });
          } else {
            this.translateService.get('ESTANCIA').subscribe((val) => {
              this.buttonDepartureConfig.title = val.CONFIGURAR_SALIDA_TITLE;
              this.buttonDepartureConfig.subtitle = (this.isTwoDaysBeforeCheckOut) ? val.CONFIGURAR_SALIDA_SUBTITLE : val.CONFIGURAR_SALIDA_SUBTITLE_DISABLED;
            });
          }
        }
      },
      (error) => {}
    );
  }

  openConfigDeparture() {
    this.navCtrl.push(ConfigurarSalidaPage, {form: this.formMyDepartureConfig});
  }

  careTeamTapped(person) {
    this.navCtrl.push(FichaTerapeutaPage, {id: person.id, role: person.role});
  }

  agendaTapped () {
    this.navCtrl.push(AgendaPage);
  }

  horariosTapped () {
    this.navCtrl.push(InstalacionesHorariosPage);
  }

  tlfTapped () {
    this.navCtrl.push(TelefonosInteresPage);
  }

  prescripcionesTapped() {
    this.navCtrl.push(FichaPrescripcionesPage, {items: this.userData.user.prescriptions});
  }

  dietaTapped () {
    this.navCtrl.push(DietaPage, {
      stay: {
        checkIn: this.userData.user.checkIn,
        checkOut: this.userData.user.checkOut,
        diet: this.userData.user.diet
      }
    });
  }

  solicitarFacturaTapped () {
    let modal = this.modalCtrl.create(SolicitarFacturaPage);
    modal.present();
  }

    /*
      console: [
        Object { value="dog", count=2}, 
        Object { value="cat", count=3}, 
        Object { value="buffalo", count=1}, 
        Object { value="wolf", count=1}, 
        Object { value="tiger", count=1}
      ]
  */
    compressArray(original) {
      let compressed = [];
      // make a copy of the input array
      let copy = original.slice(0);
     
      // first loop goes over every element
      for (let i = 0; i < original.length; i++) {
     
        let myCount = 0;  
        // loop over every element in the copy and see if it's the same
        for (let w = 0; w < copy.length; w++) {
          if (original[i] == copy[w]) {
            // increase amount of times duplicate is found
            myCount++;
            // sets item to undefined
            delete copy[w];
          }
        }
     
        if (myCount > 0) {
          let a: any = [];
          a['value'] = original[i];
          a['count'] = myCount;
          compressed.push(a);
        }
      }
      return compressed;
    };

  openActionSheet () {
    this.translateService.get('ACTION_SHEET_CONTACT').subscribe((val) => {
      let actionSheet = this.actionsheetCtrl.create({
        title: val.TITLE,
        cssClass: 'action-sheet',
        buttons: [
          {
            text: val.LLAMAR,
            icon: !this.platform.is('ios') ? 'call' : null,
            handler: () => {
              let tlfNumber = SHA_TELEPHONE.contactar || undefined;
              if(tlfNumber) {
                CallNumber.callNumber(tlfNumber, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch(() => window.open("tel:" + tlfNumber, '_system'));
              }
            }
          },
          {
            text: val.WHATSAPP,
            icon: !this.platform.is('ios') ? 'logo-whatsapp' : null,
            handler: () => {
              let tlfNumber = SHA_WHATSAPP.GR || undefined;
              if(tlfNumber) {
                window.open("whatsapp://send?phone=" + tlfNumber, '_system');
              }
            }
          },
          {
            text: val.ENVIAR_EMAIL,
            icon: !this.platform.is('ios') ? 'mail-open' : null,
            handler: () => {
              let email = SHA_EMAIL.contactar || undefined;
              if(email)
                window.open("mailto:" + email, '_system');
            }
          },
          {
            text: val.CANCELAR,
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel', // will always sort to be on the bottom
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    });
  }

  getRandom() {
    return Date.now();
  }


}
