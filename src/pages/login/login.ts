import { Component } from '@angular/core';
import { Platform, NavController, MenuController, Events} from 'ionic-angular';
import { ScreenOrientation } from 'ionic-native';
import { UserData } from '../../providers/user-data';
import { LoadingService } from '../../providers/loading-service';

import { RecuperarPasswordPage } from '../../pages/recuperar-password/recuperar-password';
import { CondicionesUsoPage } from '../../pages/condiciones-uso/condiciones-uso';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  registerCredentials: any;
  typePassField: string;
  typePassFieldText: string;
  passFieldStatus: boolean;
  acceptConditions: any;

  constructor(private navCtrl: NavController, private platform: Platform, private loading: LoadingService, private events: Events, private menuCtrl: MenuController, public userData: UserData) {
    this.registerCredentials = {email: '', password: ''};
    this.passFieldStatus = false;
  }

  ionViewDidEnter() {
    this.disableMenu();
  }

  ionViewDidLoad() {
    if(this.platform.is('cordova')) {
      ScreenOrientation.lockOrientation('portrait');
    }
  }

  ionViewDidLeave() {
    if(this.platform.is('cordova')) {
      ScreenOrientation.unlockOrientation();
    }
  }

  login() {
    this.loading.show();
    this.userData.login(this.registerCredentials).subscribe(
      result => {
        if(result !== null) {
          this.loading.showAlertRetry(result);
        } else {
          this.loading.hide();
          this.events.publish('user:loggedIn', this.userData.user.image);
        }
      },
      error => {
        this.loading.showAlert(error);
      }
    )
  }


  //desactiva el menu lateral
  disableMenu() {
    this.menuCtrl.enable(false, 'menu')
  }

  goToRecoverPassword() {
    this.navCtrl.push(RecuperarPasswordPage);
  }

  goToConditionsUse() {
    this.navCtrl.push(CondicionesUsoPage);
  }

  togglePassField () {
    this.passFieldStatus = !this.passFieldStatus;
  }
}
