import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-tratamientos-incluidos',
  templateUrl: 'tratamientos-incluidos.html'
})
export class TratamientosIncluidosPage {

  treatments: any;

  constructor(public navCtrl: NavController, private navParams: NavParams) {
    this.treatments = this.compressArray(this.navParams.get('items'));
    console.log(this.treatments);
  }

  ionViewDidLoad () {
  }


  /*
    console: [
      Object { value="dog", count=2}, 
      Object { value="cat", count=3}, 
      Object { value="buffalo", count=1}, 
      Object { value="wolf", count=1}, 
      Object { value="tiger", count=1}
    ]
*/
  compressArray(original) {
    let compressed = [];
    // make a copy of the input array
    let copy = original.slice(0);
   
    // first loop goes over every element
    for (let i = 0; i < original.length; i++) {
   
      let myCount = 0;  
      // loop over every element in the copy and see if it's the same
      for (let w = 0; w < copy.length; w++) {
        if (original[i] == copy[w]) {
          // increase amount of times duplicate is found
          myCount++;
          // sets item to undefined
          delete copy[w];
        }
      }
   
      if (myCount > 0) {
        let a: any = [];
        a['value'] = original[i];
        a['count'] = myCount;
        compressed.push(a);
      }
    }
    return compressed;
  };

}
