import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { SHA_WEB } from '../../models/models';

@Component({
  selector: 'page-ficha-categoria-shafire',
  templateUrl: 'ficha-categoria-shafire.html'
})
export class FichaCategoriaShafirePage {

  params: any;

  constructor(public navCtrl: NavController, private platform: Platform, public userData: UserData) {
    this.params = {puntos:userData.user.shafire.points.replace(",", "."),categoria:userData.user.shafire.category};
    console.log(this.userData.user.shafire);
  }

  ionViewDidLoad() {}

  openConditionsPage() {
    this.platform.ready().then(() => {
      window.open(SHA_WEB.conditions_shafire_points[this.userData.user.lang],'_blank', 'location=yes,toolbar=yes')
    });  
  }

  openPrivilegedPage() {
    this.platform.ready().then(() => {
      window.open(SHA_WEB.privileged[this.userData.user.lang],'_blank', 'location=yes,toolbar=yes')
    });    
  }

}
