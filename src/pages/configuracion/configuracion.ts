import { Component } from '@angular/core';
import { NavController, ModalController, Events, AlertController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';
import { OneSignal } from 'ionic-native';
import { UserData } from '../../providers/user-data';
import { NotificationsService } from '../../providers/notifications-service';
import { LoginPage } from '../../pages/login/login';
import { CambiarContrasenaPage } from '../../pages/cambiar-contrasena/cambiar-contrasena';
import { CondicionesUsoPage } from '../../pages/condiciones-uso/condiciones-uso';
import { LoadingService } from '../../providers/loading-service';
import { LocalStorageService } from '../../providers/local-storage-service';
/*
  Generated class for the Configuracion page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-configuracion',
  templateUrl: 'configuracion.html'
})
export class ConfiguracionPage {

  language: string;
  weekStartDay: number;
  adviceTime: string;

  constructor(public navCtrl: NavController, private localStorage : LocalStorageService, public loading: LoadingService, public modalCtrl: ModalController,private notificationService: NotificationsService, private alertCtrl: AlertController, private events: Events, private userData: UserData, private translateService: TranslateService) {
    this.language = this.userData.user.lang;
    this.weekStartDay = this.userData.user.weekStartDay;
    this.adviceTime = this.userData.user.adviceTime;
  }

  ionViewDidLoad() {
  }

  logout() {
    this.translateService.get('AJUSTES').subscribe((val) => {
      this.alertCtrl.create({
        title: val.CERRAR_SESION_MSG_TITLE,
        message: val.CERRAR_SESION_MSG_SUBTITLE,
        buttons: [
          {
            text: val.CANCELAR,
            role: 'cancel',
            handler: () => {}
          },
          {
            text: val.ACEPTAR,
            handler: () => {
              this.userData.logout().subscribe(succ => {
                this.notificationService.cancelAllNotifications();
                OneSignal.setSubscription(false);
                this.navCtrl.setRoot(LoginPage);
              })
            }
          }
        ]
      }).present();
    });
  }

  changePassword() {
    let changePasswordModal = this.modalCtrl.create(CambiarContrasenaPage);
    changePasswordModal.present();
  }

  changeLanguage(lang) {
    this.userData.setLang(lang);
    this.translateService.use(lang);
    this.notificationService.cancelAllNotifications();
    this.localStorage.setLang(lang);
    setTimeout(()=>{
      this.loading.changeLanguage();
      this.notificationService.scheduleAllNotifications();
    },200);
  }

  changeWeekStartDay() {
    this.userData.setWeekStartDay(this.weekStartDay);
  }

  changeAdviceTime() {
    this.userData.setAdviceTime(this.adviceTime)
  }

  conditions(){
    this.navCtrl.push(CondicionesUsoPage);
  }

}
