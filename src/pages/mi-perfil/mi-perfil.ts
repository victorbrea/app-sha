import { Component } from '@angular/core';
import { NavController, Platform, Events, ActionSheetController } from 'ionic-angular';
import { StatusBar, Camera } from 'ionic-native';
import { UserData } from '../../providers/user-data';
import { LoadingService } from '../../providers/loading-service';
import { FichaPrescripcionesPage } from '../ficha-prescripciones/ficha-prescripciones';
import { EstanciasAnterioresPage } from '../estancias-anteriores/estancias-anteriores';
import { FichaResumenEstanciaPage } from '../ficha-resumen-estancia/ficha-resumen-estancia';
import { FichaCategoriaShafirePage } from '../ficha-categoria-shafire/ficha-categoria-shafire';
import { TranslateService } from 'ng2-translate';
import { CallNumber } from 'ionic-native';
import { SHA_EMAIL, SHA_TELEPHONE, SHA_WHATSAPP, CACHED_IMAGES } from '../../models/models';

import moment from 'moment';

@Component({
  selector: 'page-mi-perfil',
  templateUrl: 'mi-perfil.html'
})
export class MiPerfilPage {

  moment: any;
  prescriptions: Array<any> = [];
  base64Image: any;
  showPreferences: any = false;
  cachedImages = CACHED_IMAGES;

  constructor(public navCtrl: NavController, private loading: LoadingService, private platform: Platform, public userData: UserData, private events: Events, private translateService: TranslateService, private actionsheetCtrl: ActionSheetController) {
    this.moment = moment;
  }

  ionViewDidLoad () {
    for(let stay of this.userData.user.stays) {
      this.prescriptions.splice(0,0,...stay['prescriptions']);
    }
    this.prescriptions.splice(0,0,...this.userData.user.prescriptions);
  }

  ionViewWillEnter () {
    if (this.platform.is('ios')) {
      //StatusBar.overlaysWebView(false);
    }
    StatusBar.styleLightContent();
    StatusBar.backgroundColorByHexString('#A39382');

  }

  ionViewDidLeave () {
    if (this.platform.is('ios')) {
      StatusBar.styleDefault();
      //StatusBar.backgroundColorByHexString('#ffffff');
      //StatusBar.overlaysWebView(true);
    } else {
      StatusBar.styleDefault();
      StatusBar.backgroundColorByHexString('#000000');
    }
  }

  prescripcionesTapped () {
    this.navCtrl.push(FichaPrescripcionesPage, {items: this.prescriptions});
  }

  estanciasTapped () {
    this.navCtrl.push(EstanciasAnterioresPage, {items: this.userData.user.stays});
  }

  estanciaTapped (stay) {
    this.navCtrl.push(FichaResumenEstanciaPage, {item: stay});
  }

  categoryShafireTapped() {
    this.navCtrl.push(FichaCategoriaShafirePage);
  }

  preferencesTapped() {
    this.showPreferences = !this.showPreferences;
  }

  pickPicture(){

    Camera.getPicture({
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType     : Camera.PictureSourceType.PHOTOLIBRARY,
        mediaType: Camera.MediaType.PICTURE,
        allowEdit: true
    }).then((imageData) => {
      // imageData is a base64 encoded string
        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.loading.show();
        this.userData.uploadUserImage(this.base64Image).subscribe(
          url => {
            this.loading.hide();
            this.userData.user.image = String(url);
            //lanza el evento para capturar en 'app.components.ts'(módulo padre) la imagen de usuario
            //this.events.publish('user:image', String(url));
          },
          error => {
            this.loading.showAlert(error);
          }
        ); 
    }, (err) => {
        console.log(err);
    });
  }

  openActionSheet () {
    this.translateService.get('ACTION_SHEET_CONTACT').subscribe((val) => {
      let actionSheet = this.actionsheetCtrl.create({
        title: val.TITLE,
        cssClass: 'action-sheet',
        buttons: [
          {
            text: val.LLAMAR,
            icon: !this.platform.is('ios') ? 'call' : null,
            handler: () => {
              let tlfNumber = SHA_TELEPHONE.contactar || undefined;
              if(tlfNumber) {
                CallNumber.callNumber(tlfNumber, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch(() => window.open("tel:" + tlfNumber, '_system'));
              }
            }
          },
          {
            text: val.WHATSAPP,
            icon: !this.platform.is('ios') ? 'logo-whatsapp' : null,
            handler: () => {
              let tlfNumber = SHA_WHATSAPP.GR || undefined;
              if(tlfNumber) {
                window.open("whatsapp://send?phone=" + tlfNumber, '_system');
              }
            }
          },
          {
            text: val.ENVIAR_EMAIL,
            icon: !this.platform.is('ios') ? 'mail-open' : null,
            handler: () => {
              let email = SHA_EMAIL.contactar || undefined;
              if(email)
                window.open("mailto:" + email, '_system');
            }
          },
          {
            text: val.CANCELAR,
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel', // will always sort to be on the bottom
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    });
  }

  getRandom() {
    return Date.now();
  }


}
