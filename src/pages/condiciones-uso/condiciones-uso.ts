import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

/*
  Generated class for the TerminosCondicionesSalud page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-condiciones-uso',
  templateUrl: 'condiciones-uso.html'
})
export class CondicionesUsoPage {

  constructor(public navCtrl: NavController, private viewCtrl: ViewController) {}

  ionViewDidLoad() {
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
