import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { window } from '@angular/platform-browser/src/facade/browser';


@Component({
  selector: 'page-video',
  templateUrl: 'video.html'
})
export class VideoPage implements OnInit {

  videoId: string;
  videoUrl: any;
  player: any;
  hideCloseBtn: boolean;

  constructor(private platform: Platform, private navCtrl: NavController, private navParams: NavParams, private sanitizer: DomSanitizer) {
    this.videoId = navParams.get('videoId');
    let url = "https://www.youtube.com/embed/"+this.videoId+"?enablejsapi=1&fs=0&rel=0&showinfo=0&modestbranding=1&iv_load_policy=3";
    this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    this.hideCloseBtn = true;
  }

  ngOnInit() {
    //carga la libreria Youtube
    let tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    let firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    //configura el reproductor
    this.setupPlayer();
  }

  ionViewDidLoad() {
    setTimeout(()=>{
      this.hideCloseBtn = false;
    },1000)
  }

  setupPlayer () {
    window['onYouTubeIframeAPIReady'] = () => {
      if (window['YT']) {
        this.loadPlayer();
      }
    }
    if (window.YT && window.YT.Player) {
      this.loadPlayer();
    }
  }

  loadPlayer() {
    this.player = new window.YT.Player('YT_player', {
      events: {
        'onStateChange': 
          (event) => {
            //video stop
            if (event.data == 0) {
              this.backToPage();
            }
          }
      }
    });    
  }

  backToPage() {
    this.navCtrl.pop();
  }

}
