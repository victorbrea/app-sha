import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { FichaResumenEstanciaPage } from '../ficha-resumen-estancia/ficha-resumen-estancia';
import { CACHED_IMAGES } from '../../models/models';

import moment from 'moment';

@Component({
  selector: 'page-estancias-anteriores',
  templateUrl: 'estancias-anteriores.html'
})
export class EstanciasAnterioresPage {

  moment: any;
  items: any;
  cachedImages = CACHED_IMAGES;

  constructor(public navCtrl: NavController, private navParams: NavParams, public userData: UserData) {
    this.moment = moment;
    this.items = this.navParams.get('items');
  }

  ionViewDidLoad() {}

  estanciaTapped (stay) {
    this.navCtrl.push(FichaResumenEstanciaPage, {item: stay});
  }

  getRandom() {
    return Date.now();
  }

}
