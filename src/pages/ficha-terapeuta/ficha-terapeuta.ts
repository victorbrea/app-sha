import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { StatusBar } from 'ionic-native';
import { DoctorData } from '../../providers/doctor-data';
import { LoadingService } from '../../providers/loading-service';
import { FichaTratamientoPage } from '../../pages/ficha-tratamiento/ficha-tratamiento';
import { TranslateService } from 'ng2-translate';
import { CACHED_IMAGES } from '../../models/models';

@Component({
  selector: 'page-ficha-terapeuta',
  templateUrl: 'ficha-terapeuta.html'
})
export class FichaTerapeutaPage {

  doctor: any;
  title_page: string;
  texto_defecto: string = '';
  cachedImages = CACHED_IMAGES;

  constructor(private platform: Platform, private navCtrl: NavController, private loading: LoadingService, private translateService: TranslateService, private navParams: NavParams, private doctorData: DoctorData) {
    this.doctor = {id:null,name:null,image:null,specialities:[],cv:null,treatments:[]}
    let id = this.navParams.get('id');
    let role = this.navParams.get('role');
    this.translateService.get('TERAPEUTA').subscribe((val) => {
      if(role == 'GA') {
        this.title_page = val.GA;
        this.texto_defecto = val.GA_DEFAULT_TEXT;
      } else if(role == 'GR'){
        this.title_page = val.GR;
        this.texto_defecto = val.GR_DEFAULT_TEXT;
      } 
      else if(role == 'Médico')
         this.title_page = val.MEDICO;
      else if(role == 'Consultor nutricional')
        this.title_page = val.CN;
      else
        this.title_page = role;
      this.loading.show();
      this.loadDoctor(id, role);
    })
  }

  ionViewWillEnter () {
    if (this.platform.is('ios')) {
      //StatusBar.overlaysWebView(false);
    }
    StatusBar.styleLightContent();
    StatusBar.backgroundColorByHexString('#A39382');
  }

  ionViewDidLeave () {
    if (this.platform.is('ios')) {
      StatusBar.styleDefault();
      //StatusBar.backgroundColorByHexString('#ffffff');
      //StatusBar.overlaysWebView(true);
    } else {
      StatusBar.styleDefault();
      StatusBar.backgroundColorByHexString('#000000');
    }
  }

  loadDoctor(id, role) {
    this.doctorData.getDoctor(id, role).subscribe(
      (data)=> {
        this.doctor = data;
        this.loading.hide();
      },
      (error)=> {
        this.loading.showAlert(error);
      }
    );  
  }

  treatmentTapped(treatment) {
    this.navCtrl.push(FichaTratamientoPage, {
      treatment: treatment
    });
  }

  getRandom() {
    return Date.now();
  }

}
