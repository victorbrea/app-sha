import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CACHED_IMAGES } from '../../models/models';


@Component({
  selector: 'page-ficha-consejo',
  templateUrl: 'ficha-consejo.html'
})
export class FichaConsejoPage {

  item: any;
  cachedImages = CACHED_IMAGES;

  constructor(private navCtrl: NavController, private navParams: NavParams) {
    this.item = this.navParams.get('item');
  }

  ionViewDidLoad() {}

  getRandom() {
    return Date.now();
  }

}
