import { Component, OnInit } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { TreatmentsData } from '../../providers/treatments-data';
import { LoadingService } from '../../providers/loading-service';
import { Treatment } from '../../models/models';
import { FichaTratamientoPage } from '../../pages/ficha-tratamiento/ficha-tratamiento';
import { CACHED_IMAGES } from '../../models/models';

@Component({
  selector: 'page-tratamientos',
  templateUrl: 'tratamientos.html'
})
export class TratamientosPage implements OnInit{

  treatments: Array<Treatment>;
  grid: any = [];
  cachedImages = CACHED_IMAGES;
  addTreatmentDaySelected: any;

  constructor(public navCtrl: NavController, private treatmentsData: TreatmentsData, private loading: LoadingService, private navParams: NavParams) {
    
    this.treatments = this.navParams.get('treatments');
    this.addTreatmentDaySelected = this.navParams.get('day');
    this.createGrid();
  }

  ngOnInit() {}

  ionViewDidLoad() {}

  createGrid() {
    let rowNum = 0;
    for (let i = 0; i < this.treatments.length; i+=2) {
      this.grid[rowNum] = Array(2);
      if (this.treatments[i]) {
        this.grid[rowNum][0] = this.treatments[i]
      }
      if (this.treatments[i+1]) {
        this.grid[rowNum][1] = this.treatments[i+1]
      }
      rowNum++;
    }    
  }

  treatmentTapped(event, treatment) {
    this.navCtrl.push(FichaTratamientoPage, {
      treatment: treatment,
      day: this.addTreatmentDaySelected
    });
  }

  getRandom() {
    return Date.now();
  }

}
