import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { NotificationsService } from '../../providers/notifications-service';
import { TranslateService } from 'ng2-translate';
import { FichaTerapeutaPage } from '../ficha-terapeuta/ficha-terapeuta';
import { VideoPage } from '../video/video';
import { CACHED_IMAGES } from '../../models/models';

import moment from 'moment';


@Component({
  selector: 'page-cita',
  templateUrl: 'cita.html'
})
export class CitaPage implements OnInit{

  cita: any;
  notificar: any;
  day: any;
  showReminder: any;
  dateToSchedule: any;
  moment: any;
  advice: string;
  cachedImages = CACHED_IMAGES;

  constructor(private navCtrl: NavController, private navParams: NavParams, private notificationsService: NotificationsService ,public userData: UserData, private alert: AlertController, private translateService: TranslateService) {
    this.cita = navParams.get('cita');
    let adviceTimeSplit = this.userData.user.adviceTime.split('-');
    this.translateService.get('AJUSTES').subscribe((val) => {
      this.advice = adviceTimeSplit[0] + " " + val[adviceTimeSplit[1].toUpperCase()];
    });
    let units:moment.unitOfTime.DurationConstructor = (adviceTimeSplit[1].charAt(0) == 'm') ? 'minute' : ((adviceTimeSplit[1].charAt(0) == 'h') ? 'hour' : 'day');
    this.dateToSchedule = moment(`${this.cita.day} ${this.cita.start}`,"DD/MM/YYYY HH:mm").locale(this.userData.user.lang).subtract(parseInt(adviceTimeSplit[0]), units);
    this.showReminder = moment().isBefore(this.dateToSchedule);
    //unix() para extraer la fecha en formato numérico timeStamp. Corresponde al id de la notificacion
    this.notificationsService.isScheduledCitaReminder(this.dateToSchedule.clone().unix()).then(val=> {
      this.notificar = val || false; 
    })
    this.moment = moment;
  }

  ngOnInit() {
    let lang = this.userData.user.lang;
    let de = (lang === 'es') ? 'de' : '';
    this.day = moment(this.cita.day,"DD MM YYYY").locale(lang).format(`dddd, D [${de}] MMMM [${de}] YYYY`);
  }

  ionViewDidLoad() {
  }

  doctorTapped () {
    this.navCtrl.push(FichaTerapeutaPage, {id: this.cita.doctor.id, role: "Profesional"});
  }

  toggleButtonTapped(event){
    this.notificar = event.checked;
    if(this.notificar) {
      this.notificationsService.scheduleCitaReminder(this.cita, this.dateToSchedule);
    } else {
      this.notificationsService.cancelReminderCita(this.dateToSchedule.clone().unix());
    }
  }

  openVideo() {
    if (navigator.onLine) {
      this.navCtrl.push(VideoPage, {videoId: this.cita.videoId})
    }
  }

  getRandom() {
    return Date.now();
  }

}
