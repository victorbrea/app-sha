import { Component, OnInit } from '@angular/core';
import { NavController, Events, ActionSheetController, Platform } from 'ionic-angular';
import { NewsletterData } from '../../providers/newsletter-data';
import { LoadingService } from '../../providers/loading-service';
import { UserData } from '../../providers/user-data';
import { WeatherLocationService } from '../../providers/weather-location-service';
import { TranslateService } from 'ng2-translate';
import { CallNumber } from 'ionic-native';
import { FichaTratamientoPage } from '../ficha-tratamiento/ficha-tratamiento';
import { FichaDetallesNewsletterPage } from '../ficha-detalles-newsletter/ficha-detalles-newsletter';
import { FichaDetallesAcademyPage } from '../ficha-detalles-academy/ficha-detalles-academy';

import { SHA_EMAIL, SHA_TELEPHONE, SHA_WHATSAPP, CACHED_IMAGES } from '../../models/models';

import moment from 'moment';

@Component({
  selector: 'page-newsletter',
  templateUrl: 'newsletter.html'
})
export class NewsletterPage implements OnInit{

  date: any;
  meteo: any;
  newsletter: any;
  moment: any;
  cachedImages = CACHED_IMAGES;

  constructor(private navCtrl: NavController, private platform: Platform, private events: Events, private translateService: TranslateService, private userData: UserData, private weather: WeatherLocationService, private loading: LoadingService, private newsletterData: NewsletterData, private actionsheetCtrl: ActionSheetController) {
    this.events.publish('menu:newsletter');
    this.date = ""; 
    this.moment = moment;
  }

  ngOnInit () {}

  ionViewWillEnter () {
    this.loading.show();
    this.newsletterData.getNewsletter().subscribe(
      result => {
        this.loading.hide();
        if(result) {
          this.newsletter = result;
          console.log(this.newsletter)
          this.date = moment(result['date'] + " 12:00", "DD/MM/YYYY HH:mm").locale(this.userData.user.lang).format('LLLL').split(" 12:00")[0];
          //this.getMeteo();
        }
        //this.getMeteo();
      },
      error => {
        this.loading.showAlert(error);
      }
    ); 
  }

  getMeteo () {
    if(moment().format("DD/MM/YYYY") == this.newsletter.date)
      this.weather.getWeatherToday().then(data=> this.meteo = data);
    else if(moment().add(1, 'day').format("DD/MM/YYYY") == this.newsletter.date)
      this.weather.getWeatherTomorrow().then(data=> this.meteo = data);
    else
      this.meteo = undefined;
  }

  openTreatment (treatment) {
    this.navCtrl.push(FichaTratamientoPage, {treatment: treatment})
  }

  openUrl (url) {
    this.platform.ready().then(() => {
      window.open(url,'_blank', 'location=yes,toolbar=yes')
    });
  }

  infoTapped(item, section) {
    if (section == 'SHA Academy')
      this.navCtrl.push(FichaDetallesAcademyPage, {activity:item});
    else if (section == 'Generic')
      this.navCtrl.push(FichaDetallesNewsletterPage, {item:item, section:'SHA Academy'});
    else
      this.navCtrl.push(FichaDetallesNewsletterPage, {item:item, section:section});
  }

  openActionSheet () {
    this.translateService.get('ACTION_SHEET_CONTACT').subscribe((val) => {
      let actionSheet = this.actionsheetCtrl.create({
        title: val.TITLE,
        cssClass: 'action-sheet',
        buttons: [
          {
            text: val.LLAMAR,
            icon: !this.platform.is('ios') ? 'call' : null,
            handler: () => {
              let tlfNumber = SHA_TELEPHONE.contactar || undefined;
              if(tlfNumber) {
                CallNumber.callNumber(tlfNumber, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch(() => window.open("tel:" + tlfNumber, '_system'));
              }
            }
          },
          {
            text: val.WHATSAPP,
            icon: !this.platform.is('ios') ? 'logo-whatsapp' : null,
            handler: () => {
              let tlfNumber = SHA_WHATSAPP.GR || undefined;
              if(tlfNumber) {
                window.open("whatsapp://send?phone=" + tlfNumber, '_system');
              }
            }
          },
          {
            text: val.ENVIAR_EMAIL,
            icon: !this.platform.is('ios') ? 'mail-open' : null,
            handler: () => {
              let email = SHA_EMAIL.contactar || undefined;
              if(email)
                window.open("mailto:" + email, '_system');
            }
          },
          {
            text: val.CANCELAR,
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel', // will always sort to be on the bottom
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    });
  }

  getRandom() {
    return Date.now();
  }

}
