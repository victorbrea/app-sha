import { Component } from '@angular/core';
import { NavController, MenuController, Events } from 'ionic-angular';


@Component({
  selector: 'page-sin-conexion',
  templateUrl: 'sin-conexion.html'
})
export class SinConexionPage {

  spinnerHide: boolean = true;

  constructor(public navCtrl: NavController, private menuCtrl: MenuController, public events: Events) {}

  ionViewDidLoad() {
    
  }

  ionViewDidEnter() {
    this.disableMenu();
  }

  //desactiva el menu lateral
  disableMenu() {
    this.menuCtrl.enable(false, 'menu');
  }

  reload() {
    this.spinnerHide = false;
    setTimeout(() => {
      this.spinnerHide = true;
      this.events.publish('initializeApp');
    }, 1250);
  }

}
