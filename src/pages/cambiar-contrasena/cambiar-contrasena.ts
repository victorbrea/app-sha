import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';
import { LoadingService } from '../../providers/loading-service';
import { UserData } from '../../providers/user-data';


@Component({
  selector: 'page-cambiar-contrasena',
  templateUrl: 'cambiar-contrasena.html'
})
export class CambiarContrasenaPage {

  form: any;

  constructor(public navCtrl: NavController, private viewCtrl: ViewController, private translateService: TranslateService, private userData: UserData, private loading: LoadingService) {
    this.form = {
      actual: "",
      new: "",
      new_bis: ""
    }
  }

  ionViewDidLoad() {}

  dismiss() {
    this.viewCtrl.dismiss();
  }

  submit() {
    this.translateService.get('CAMBIAR_CONTRASENA').subscribe((val) => {
      if (this.form.new != this.form.new_bis) {
        this.loading.showAlert(val.ERROR_COMPARAR_SUBTITLE, val.ERROR_COMPARAR_TITLE);
        this.form.new = "";
        this.form.new_bis = "";
      } else {
        this.loading.show();
        this.userData.changePassword(this.form.actual, this.form.new).subscribe(
          result => {
            this.loading.hide();
            if(result) {
              this.loading.showToast(val.SUCCESS_MSG);
              this.dismiss();
            } else {
              this.loading.showAlert(val.ERROR_PASSWORD_SUBTITLE,val.ERROR_PASSWORD_TITLE);
            }
          },
          error => {
            this.loading.showAlert(error);
          }
        )
      }
    });
  }

}
