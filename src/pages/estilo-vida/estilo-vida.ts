import { Component } from '@angular/core';
import { NavController, ActionSheetController, Platform } from 'ionic-angular';
import { SHA_WEB } from '../../models/models';

import { UserData } from '../../providers/user-data';
import { TranslateService } from 'ng2-translate';
import { CallNumber } from 'ionic-native';
import { SHA_EMAIL, SHA_TELEPHONE, SHA_WHATSAPP } from '../../models/models';


@Component({
  selector: 'page-estilo-vida',
  templateUrl: 'estilo-vida.html'
})
export class EstiloVidaPage {

  constructor(public navCtrl: NavController, private platform: Platform, public userData: UserData, private translateService: TranslateService, private actionsheetCtrl: ActionSheetController) {}

  ionViewDidLoad () {}

  boutiqueTapped () {
    this.platform.ready().then(() => {
      window.open(SHA_WEB.boutique[this.userData.user.lang],'_blank', 'location=yes,toolbar=yes')
    });  
  }

  magazineTapped () {
    this.platform.ready().then(() => {
      window.open(SHA_WEB.magazine[this.userData.user.lang],'_blank', 'location=yes,toolbar=yes')
    });  
  }

  privilegedTapped () {
    this.platform.ready().then(() => {
      window.open(SHA_WEB.privileged[this.userData.user.lang],'_blank', 'location=yes,toolbar=yes')
    });  
  }

  foundationTapped () {
    this.platform.ready().then(() => {
      window.open(SHA_WEB.foundation[this.userData.user.lang],'_blank', 'location=yes,toolbar=yes')
    });  
  }

  esenzaTapped () {
    this.platform.ready().then(() => {
      window.open(SHA_WEB.esenza[this.userData.user.lang],'_blank', 'location=yes,toolbar=yes')
    });  
  }

  abTapped () {
    this.platform.ready().then(() => {
      window.open(SHA_WEB.ab[this.userData.user.lang],'_blank', 'location=yes,toolbar=yes')
    });  
  }

  programasTapped () {
    this.platform.ready().then(() => {
      window.open(SHA_WEB.programas[this.userData.user.lang],'_blank', 'location=yes,toolbar=yes')
    });  
  }

  tratamientosTapped () {
    this.platform.ready().then(() => {
      window.open(SHA_WEB.tratamientos[this.userData.user.lang],'_blank', 'location=yes,toolbar=yes')
    });  
  }

  openActionSheet () {
    this.translateService.get('ACTION_SHEET_CONTACT').subscribe((val) => {
      let actionSheet = this.actionsheetCtrl.create({
        title: val.TITLE,
        cssClass: 'action-sheet',
        buttons: [
          {
            text: val.LLAMAR,
            icon: !this.platform.is('ios') ? 'call' : null,
            handler: () => {
              let tlfNumber = SHA_TELEPHONE.contactar || undefined;
              if(tlfNumber) {
                CallNumber.callNumber(tlfNumber, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch(() => window.open("tel:" + tlfNumber, '_system'));
              }
            }
          },
          {
            text: val.WHATSAPP,
            icon: !this.platform.is('ios') ? 'logo-whatsapp' : null,
            handler: () => {
              let tlfNumber = SHA_WHATSAPP.GR || undefined;
              if(tlfNumber) {
                window.open("whatsapp://send?phone=" + tlfNumber, '_system');
              }
            }
          },
          {
            text: val.ENVIAR_EMAIL,
            icon: !this.platform.is('ios') ? 'mail-open' : null,
            handler: () => {
              let email = SHA_EMAIL.contactar || undefined;
              if(email)
                window.open("mailto:" + email, '_system');
            }
          },
          {
            text: val.CANCELAR,
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel', // will always sort to be on the bottom
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    });
  }

}
