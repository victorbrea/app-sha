import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { CACHED_IMAGES } from '../../models/models';

import moment from 'moment';

@Component({
  selector: 'page-ficha-prescripciones',
  templateUrl: 'ficha-prescripciones.html'
})
export class FichaPrescripcionesPage {

  moment: any;
  items: any;
  cachedImages = CACHED_IMAGES;

  constructor(public navCtrl: NavController, private navParams: NavParams, public userData: UserData) {
    this.moment = moment;
    this.items = this.navParams.get('items');
  }

  ionViewDidLoad() {}

  getRandom() {
    return Date.now();
  }

}
