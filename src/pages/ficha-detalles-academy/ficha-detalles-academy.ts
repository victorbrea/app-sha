import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CACHED_IMAGES } from '../../models/models';

import moment from 'moment';

/*
  Generated class for the FichaDetallesProgramaPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-ficha-detalles-academy',
  templateUrl: 'ficha-detalles-academy.html'
})
export class FichaDetallesAcademyPage {

  item: any;
  moment: any;
  cachedImages = CACHED_IMAGES;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.item = this.navParams.get('activity');
    this.moment = moment;
  }

  ionViewDidLoad() {}

  getRandom() {
    return Date.now();
  }

}
