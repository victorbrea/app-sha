import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, Events, Platform, ActionSheetController } from 'ionic-angular';
import { LocalNotifications } from 'ionic-native';
import { TranslateService } from 'ng2-translate';
import { UserData } from '../../providers/user-data';
import { NotificationsService } from '../../providers/notifications-service';
import { LoadingService } from '../../providers/loading-service';
import { TreatmentsData } from '../../providers/treatments-data';

import { CuestionarioSaludPage } from '../../pages/cuestionario-salud/cuestionario-salud';
import { CuestionarioSatisfaccionPage } from '../../pages/cuestionario-satisfaccion/cuestionario-satisfaccion';
import { FichaConsejoPage } from '../../pages/ficha-consejo/ficha-consejo';
import { TratamientosPage } from '../../pages/tratamientos/tratamientos';
import { AgendaPage } from '../../pages/agenda/agenda';
import { NewsletterPage } from '../../pages/newsletter/newsletter';
import { MiLlegadaPage } from '../../pages/mi-llegada/mi-llegada';
import { MiEstanciaPage } from '../../pages/mi-estancia/mi-estancia';
import { ConfigurarSalidaPage } from '../../pages/configurar-salida/configurar-salida';
import { CallNumber } from 'ionic-native';
import { SHA_EMAIL, SHA_TELEPHONE, SHA_WHATSAPP } from '../../models/models';


@Component({
  selector: 'page-notificaciones',
  templateUrl: 'notificaciones.html'
})
export class NotificacionesPage implements OnInit{

  fase: string;
  arrayNotifications: any;

  constructor(public navCtrl: NavController, public loading: LoadingService, private events: Events, private platform: Platform, private treatmentsData: TreatmentsData, public userData: UserData, private notificationsService: NotificationsService, private alertCtrl: AlertController, private translateService: TranslateService, private actionsheetCtrl: ActionSheetController) {
    this.fase = userData.user.fase;
  }

  ngOnInit() {}

  ionViewDidLoad() {
    this.arrayNotifications = this.notificationsService.notificationsToShow;
    LocalNotifications.clearAll();
  }

  ionViewDidLeave() {
    this.notificationsService.markViewedAllNotificationsToShow();
  }

  private goToCuestionario(page) {
    this.translateService.get('NOTIFICACIONES').subscribe((val) => {
      this.alertCtrl.create({
        title: val.CONFIRM_TITLE,
        message: val.CONFIRM_MSG,
        buttons: [
          {
            text: val.CANCELAR,
            role: 'cancel',
            handler: () => {}
          },
          {
            text: val.ACEPTAR,
            handler: () => {
              this.navCtrl.push(page);
            }
          }
        ]
      }).present();
    });
  }

  notificationTapped($event, notification) {
    switch (notification.data.type) {
      case "cuestionarioSalud":
        this.goToCuestionario(CuestionarioSaludPage);
        break;
      case "consejo":
        this.navCtrl.push(FichaConsejoPage, {
          item: notification.data
        })
        break;
      case "agenda":
        this.navCtrl.setRoot(AgendaPage);
        break;
      case "newsletter":
        this.navCtrl.setRoot(NewsletterPage);
        break;
      case "configurarSalida":
        this.userData.getDeparture().subscribe(
          (form) => {
            this.navCtrl.setPages([MiEstanciaPage, {page: ConfigurarSalidaPage, params: {form: form}}]);
            this.events.publish('menu:miEstancia');
          },
          (error) => {}
        );
        break;
      case "tratamientoBelleza":
        this.treatmentsData.getBeautyTreatments().subscribe(
          (treatments) => {
            this.navCtrl.setPages([MiEstanciaPage, {page: TratamientosPage, params: {treatments: treatments}}]);
            this.events.publish('menu:miEstancia');
          },
          (error) => {}
        );
        break;
      case "encuesta":
        this.navCtrl.setRoot(CuestionarioSatisfaccionPage);
        break;
      case "encuesta_reminder":
        this.navCtrl.setRoot(CuestionarioSatisfaccionPage);
        break;



    }
  }

  confirmacionLlegadaTapped(value) {
    this.notificationsService.cancelConfirmMiLlegadaNotification();
    this.loading.show();
    this.userData.confirmArrival(value).subscribe(
      (msg) => {
        this.loading.showToast(msg);
        this.navCtrl.setRoot(MiLlegadaPage);
      },
      (error) => this.loading.showAlert(error)
    );
  }

  openActionSheet () {
    this.translateService.get('ACTION_SHEET_CONTACT').subscribe((val) => {
      let actionSheet = this.actionsheetCtrl.create({
        title: val.TITLE,
        cssClass: 'action-sheet',
        buttons: [
          {
            text: val.LLAMAR,
            icon: !this.platform.is('ios') ? 'call' : null,
            handler: () => {
              let tlfNumber = SHA_TELEPHONE.contactar || undefined;
              if(tlfNumber) {
                CallNumber.callNumber(tlfNumber, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch(() => window.open("tel:" + tlfNumber, '_system'));
              }
            }
          },
          {
            text: val.WHATSAPP,
            icon: !this.platform.is('ios') ? 'logo-whatsapp' : null,
            handler: () => {
              let tlfNumber = SHA_WHATSAPP.GR || undefined;
              if(tlfNumber) {
                window.open("whatsapp://send?phone=" + tlfNumber, '_system');
              }
            }
          },
          {
            text: val.ENVIAR_EMAIL,
            icon: !this.platform.is('ios') ? 'mail-open' : null,
            handler: () => {
              let email = SHA_EMAIL.contactar || undefined;
              if(email)
                window.open("mailto:" + email, '_system');
            }
          },
          {
            text: val.CANCELAR,
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel', // will always sort to be on the bottom
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    });
  }

}
