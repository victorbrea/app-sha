import { Component, ViewChild, ElementRef, trigger, state, style, animate, transition } from '@angular/core';
import { NavController, Platform, ActionSheetController } from 'ionic-angular';
import { CalendarByWeekComponent } from '../../components/calendar-by-week/calendar-by-week';
import { ShaAcademyData } from '../../providers/sha-academy-data';
import { LoadingService } from '../../providers/loading-service';
import { UserData } from '../../providers/user-data';
import { TranslateService } from 'ng2-translate';
import { CallNumber } from 'ionic-native';
import { FichaDetallesAcademyPage } from '../ficha-detalles-academy/ficha-detalles-academy';
import { SHA_EMAIL, SHA_TELEPHONE, SHA_WHATSAPP, CACHED_IMAGES } from '../../models/models';

import moment from 'moment';

@Component({
  selector: 'page-sha-academy',
  templateUrl: 'sha-academy.html',
  animations: [
    trigger('fadeInOut', [
      state('void', style({ opacity: '0' })),
      state('*', style({ opacity: '1' })),
      transition('void => *', animate('200ms ease-in'))
    ])
  ]
})

export class ShaAcademyPage {

  @ViewChild(CalendarByWeekComponent) calendar: CalendarByWeekComponent;
  @ViewChild('headerTag') headerTag: ElementRef;
  @ViewChild('scrollableTag') scrollableTag: ElementRef;
  moment = moment;
  allActivities:any = [];
  activitiesToShow:any = [];
  daySelected: string;
  noActivitiesToShow: any = false;
  cachedImages = CACHED_IMAGES;


  constructor(public navCtrl: NavController, public platform: Platform, public userData: UserData, private shaAcademyData: ShaAcademyData, private loading: LoadingService, private translateService: TranslateService, private actionsheetCtrl: ActionSheetController) {
    this.daySelected = null;
  }

  ionViewDidLoad() {
  }

  ionViewDidEnter() {
    this.loading.show();
    this.shaAcademyData.getAllActivities().subscribe(
      result => {
        console.log(result);
        this.loading.hide();
        this.allActivities = result || [];
        this.dayChanged (this.daySelected);
      },
      error => {
        this.loading.showAlert(error);
      }
    ); 
    this.resize();
  }
  
  resize() {
    let offset = this.headerTag.nativeElement.offsetHeight;
    (<HTMLDivElement>this.scrollableTag.nativeElement).style.marginTop = offset + 'px';
  }

  goToToday () {
    this.calendar.goToToday();
  }

  dayChanged (day) {
    if(this.daySelected) {
      let activitiesToDay = this.allActivities.filter((activity) => {return activity['day'] === day});
      this.activitiesToShow = (activitiesToDay.length != 0) ? activitiesToDay[0].activities : [];
      this.noActivitiesToShow = (this.activitiesToShow.length == 0) ? true : false;
    }
    this.daySelected = day;
  }

  openActivityDetails(activity) {
    this.navCtrl.push(FichaDetallesAcademyPage, {activity});
  }


  openActionSheet () {
    this.translateService.get('ACTION_SHEET_CONTACT').subscribe((val) => {
      let actionSheet = this.actionsheetCtrl.create({
        title: val.TITLE,
        cssClass: 'action-sheet',
        buttons: [
          {
            text: val.LLAMAR,
            icon: !this.platform.is('ios') ? 'call' : null,
            handler: () => {
              let tlfNumber = SHA_TELEPHONE.contactar || undefined;
              if(tlfNumber) {
                CallNumber.callNumber(tlfNumber, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch(() => window.open("tel:" + tlfNumber, '_system'));
              }
            }
          },
          {
            text: val.WHATSAPP,
            icon: !this.platform.is('ios') ? 'logo-whatsapp' : null,
            handler: () => {
              let tlfNumber = SHA_WHATSAPP.GR || undefined;
              if(tlfNumber) {
                window.open("whatsapp://send?phone=" + tlfNumber, '_system');
              }
            }
          },
          {
            text: val.ENVIAR_EMAIL,
            icon: !this.platform.is('ios') ? 'mail-open' : null,
            handler: () => {
              let email = SHA_EMAIL.contactar || undefined;
              if(email)
                window.open("mailto:" + email, '_system');
            }
          },
          {
            text: val.CANCELAR,
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel', // will always sort to be on the bottom
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    });
  }

  getRandom() {
    return Date.now();
  }

}
