import { Component } from '@angular/core';
import { NavController, ActionSheetController, Platform } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';
import { UserData } from '../../providers/user-data';
import { LoadingService } from '../../providers/loading-service';
import { LaunchNavigator, LaunchNavigatorOptions, CallNumber } from 'ionic-native';
import { SHA_EMAIL, SHA_LOCATION, SHA_TELEPHONE, SHA_WHATSAPP } from '../../models/models';


@Component({
  selector: 'page-comentar',
  templateUrl: 'comentar.html'
})
export class ComentarPage {

  form: any;

  constructor(public navCtrl: NavController, public platform: Platform, public userData: UserData, public loading: LoadingService, public translateService: TranslateService, public actionsheetCtrl: ActionSheetController) {
    this.form = {
      subject: '', 
      body: ''
    };
  }

  ionViewDidLoad() {}

  cancelarTapped($event) {
    this.navCtrl.pop();
  }

  submitComment() {
    let params = {
      userId: this.userData.user.id,
      userName: this.userData.user.name,
      bookingId: this.userData.user.bookingId,
      subject: this.form.subject,
      body: this.form.body
    }
    this.loading.show();
    this.userData.sendComment(params).subscribe(
      (msg) => {
        this.loading.showToast(msg);
        this.navCtrl.pop();
      },
      (error) => this.loading.showAlert(error)
    );
  }


  openActionSheet () {
    this.translateService.get('ACTION_SHEET_CONTACT').subscribe((val) => {
      let actionSheet = this.actionsheetCtrl.create({
        title: val.TITLE,
        cssClass: 'action-sheet',
        buttons: [
          {
            text: val.LLAMAR,
            icon: !this.platform.is('ios') ? 'call' : null,
            handler: () => {
              let tlfNumber = SHA_TELEPHONE.agenda || undefined;
              if(tlfNumber) {
                CallNumber.callNumber(tlfNumber, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch(() => window.open("tel:" + tlfNumber, '_system'));
              }
            }
          },
          {
            text: val.WHATSAPP,
            icon: !this.platform.is('ios') ? 'logo-whatsapp' : null,
            handler: () => {
              let tlfNumber = SHA_WHATSAPP.GA || undefined;
              if(tlfNumber) {
                window.open("whatsapp://send?phone=" + tlfNumber, '_system');
              }
            }
          },
          {
            text: val.ENVIAR_EMAIL,
            icon: !this.platform.is('ios') ? 'mail-open' : null,
            handler: () => {
              let email = SHA_EMAIL.reservas || undefined;
              if(email)
                window.open("mailto:" + email, '_system');
            }
          },
          {
            text: val.CANCELAR,
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel', // will always sort to be on the bottom
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    });
  }


}
