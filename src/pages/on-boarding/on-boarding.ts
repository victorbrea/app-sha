import { Component } from '@angular/core';
import { NavController, MenuController, Events } from 'ionic-angular';
import { LocalStorageService } from '../../providers/local-storage-service';
import { CuestionarioSaludPage } from '../../pages/cuestionario-salud/cuestionario-salud';
import { MiLlegadaPage } from '../../pages/mi-llegada/mi-llegada';
import { MiEstanciaPage } from '../../pages/mi-estancia/mi-estancia';
import { UserData } from '../../providers/user-data';
import { TranslateService } from 'ng2-translate';
import { EstanciasAnterioresPage } from '../../pages/estancias-anteriores/estancias-anteriores';

@Component({
  selector: 'page-on-boarding',
  templateUrl: 'on-boarding.html'
})
export class OnBoardingPage {

  slides: any;
  slidesPreSHA: any;
  slidesSHA: any;
  slidesPostSHA: any;
  slideOptions: any;
  slideCuestionarioSalud: any;
  sendItCuestionarioSalud: any;

  constructor(private navCtrl: NavController, private menuCtrl: MenuController, private translateService: TranslateService, private events: Events, private localStorage: LocalStorageService, private userData: UserData) {

    this.slideOptions = {
      initialSlide: 0,
      pager: true
    };

    this.translateService.get('SLIDERS').subscribe((val) => {

      this.slidesPreSHA = [
        {
          title: val.SLIDER_PRE.SLIDE_1.TITULO,
          description: val.SLIDER_PRE.SLIDE_1.DESCRIPCION,
          image: "assets/icon/OnBoarding-PreSha-01.png",
          button: ''
        },
        {
          title: val.SLIDER_PRE.SLIDE_2.TITULO,
          description: val.SLIDER_PRE.SLIDE_2.DESCRIPCION,
          image: "assets/icon/OnBoarding-PreSha-02.png",
          button: ''
        },
        {
          title: val.SLIDER_PRE.SLIDE_3.TITULO,
          description: val.SLIDER_PRE.SLIDE_3.DESCRIPCION,
          image: "assets/icon/OnBoarding-PreSha-03.png",
          button: ''
        },
        {
          title: val.SLIDER_PRE.SLIDE_4.TITULO,
          description: val.SLIDER_PRE.SLIDE_4.DESCRIPCION,
          image: "assets/icon/OnBoarding-PreSha-04.png",
          button: val.SLIDER_PRE.SLIDE_4.BOTON
        }
      ];

      this.slideCuestionarioSalud = {
        title: val.SLIDER_PRE.SLIDE_CUESTIONARIO.TITULO,
        description: val.SLIDER_PRE.SLIDE_CUESTIONARIO.DESCRIPCION,
        image: "assets/icon/OnBoarding-PreSha-05.png",
        button: val.SLIDER_PRE.SLIDE_CUESTIONARIO.BOTON
      }

      this.slidesSHA = [
        {
          title: val.SLIDER_SHA.SLIDE_1.TITULO,
          description: val.SLIDER_SHA.SLIDE_1.DESCRIPCION,
          image: "assets/icon/OnBoarding-Sha-01.png",
          button: ''
        },
        {
          title: val.SLIDER_SHA.SLIDE_2.TITULO,
          description: val.SLIDER_SHA.SLIDE_2.DESCRIPCION,
          image: "assets/icon/OnBoarding-Sha-02.png",
          button: ''
        },
        {
          title: val.SLIDER_SHA.SLIDE_3.TITULO,
          description: val.SLIDER_SHA.SLIDE_3.DESCRIPCION,
          image: "assets/icon/OnBoarding-Sha-03.png",
          button: ''
        },
        {
          title: val.SLIDER_SHA.SLIDE_4.TITULO,
          description: val.SLIDER_SHA.SLIDE_4.DESCRIPCION,
          image: "assets/icon/OnBoarding-Sha-04.png",
          button: val.SLIDER_SHA.SLIDE_4.BOTON
        }
      ];

      this.slidesPostSHA = [
        {
          title: val.SLIDER_POST.SLIDE_1.TITULO,
          description: val.SLIDER_POST.SLIDE_1.DESCRIPCION,
          image: "assets/icon/OnBoarding-PostSha-01.png",
          button: ''
        },
        {
          title: val.SLIDER_POST.SLIDE_2.TITULO,
          description: val.SLIDER_POST.SLIDE_2.DESCRIPCION,
          image: "assets/icon/OnBoarding-PostSha-02.png",
          button: ''
        },
        {
          title: val.SLIDER_POST.SLIDE_3.TITULO,
          description: val.SLIDER_POST.SLIDE_3.DESCRIPCION,
          image: "assets/icon/OnBoarding-PostSha-03.png",
          button: ''
        },
        {
          title: val.SLIDER_POST.SLIDE_4.TITULO,
          description: val.SLIDER_POST.SLIDE_4.DESCRIPCION,
          description_2: val.SLIDER_POST.SLIDE_4.DESCRIPCION_2,
          image: "assets/icon/OnBoarding-PostSha-04.png",
          button: val.SLIDER_POST.SLIDE_4.BOTON
        }
      ];

    });

  }

  ionViewDidLoad () {
    this.localStorage.setOnBoarding(this.userData.user.fase);

    switch (this.userData.user.fase) {

      case "preSha":
        this.slides = this.slidesPreSHA;
        this.userData.isSentCuestionarioSalud().subscribe(sendIt=> {
          this.sendItCuestionarioSalud = sendIt;
          if (!sendIt) {
            this.slides.pop();
            this.slides.push(this.slideCuestionarioSalud);
          }
        });
        break;

      case "Sha":
        this.slides = this.slidesSHA;
        break;

      case "postSha":
        this.slides = this.slidesPostSHA;
        break;
    }
  }

  skipTapped () {
    this.events.publish('user:loggedIn');
  }

  buttonTapped(action) {

    switch (this.userData.user.fase) {

      case "preSha":
        this.userData.isSentCuestionarioSalud().subscribe(sendIt=> {
          this.sendItCuestionarioSalud = sendIt;
          if (!this.sendItCuestionarioSalud && (action != 'skip')) {
            this.navCtrl.setPages([MiLlegadaPage, CuestionarioSaludPage]);
            this.events.publish('menu:miLlegada');
          } else {
            this.menuCtrl.open("menu");
            this.navCtrl.setPages([MiLlegadaPage]);
            this.events.publish('menu:miLlegada');
          }
        });
        break;

      case "Sha":
        this.menuCtrl.open("menu");
        this.navCtrl.setPages([MiEstanciaPage]);
        this.events.publish('menu:miEstancia');
        break;

      case "postSha":
        this.menuCtrl.open("menu");
        this.navCtrl.setPages([EstanciasAnterioresPage]);
        this.events.publish('menu:estanciasAnterioresPage');
        break;
    }
  }

}
