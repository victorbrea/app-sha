import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FichaTerapeutaPage } from '../ficha-terapeuta/ficha-terapeuta';
import { TratamientosIncluidosPage } from '../tratamientos-incluidos/tratamientos-incluidos';
import { FichaPrescripcionesPage } from '../ficha-prescripciones/ficha-prescripciones';
import { FichaDetallesProgramaPage } from '../ficha-detalles-programa/ficha-detalles-programa';
import { DietaPage } from '../dieta/dieta';
import { UserData } from '../../providers/user-data';
import { CACHED_IMAGES } from '../../models/models';

import moment from 'moment'; 


@Component({
  selector: 'page-ficha-resumen-estancia',
  templateUrl: 'ficha-resumen-estancia.html'
})
export class FichaResumenEstanciaPage {

  moment: any;
  stay: any;
  showEquipment: any = false;
  cachedImages = CACHED_IMAGES;
  showPrograms: any = false;


  constructor(public navCtrl: NavController, private navParams: NavParams, public userData: UserData) {
    this.stay = this.navParams.get('item');
    console.log(this.stay);
    this.moment = moment;
  }

  ionViewDidLoad () {}

  terapeutaTapped (person) {
    this.navCtrl.push(FichaTerapeutaPage, {id: person.id, role: person.role});
  }

  tratamientosTapped () {
    this.navCtrl.push(TratamientosIncluidosPage, {items: this.stay.treatmentsList});
  }

  prescripcionesTapped() {
    this.navCtrl.push(FichaPrescripcionesPage, {items: this.stay.prescriptions});
  }

  dietaTapped () {
    this.navCtrl.push(DietaPage, {stay: this.stay});
  }

  careTeamTapped() {
    this.showEquipment = !this.showEquipment;
  }

  programsTapped() {
    this.showPrograms = !this.showPrograms;
  }

  openProgramDetails(program) {
    this.navCtrl.push(FichaDetallesProgramaPage, {program});
  }

  getRandom() {
    return Date.now();
  }

}
