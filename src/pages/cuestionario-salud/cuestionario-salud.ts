import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, Content } from 'ionic-angular';
import { LocalStorageService } from '../../providers/local-storage-service';
import { UserData } from '../../providers/user-data';
import { LoadingService } from '../../providers/loading-service';
import { NotificationsService } from '../../providers/notifications-service';
import { PoliticaPrivacidadSaludPage } from '../politica-privacidad-salud/politica-privacidad-salud';
import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'page-cuestionario-salud',
  templateUrl: 'cuestionario-salud.html'
})
export class CuestionarioSaludPage {
  @ViewChild(Content) content: Content;
  arrayForm: any;
  form: any;
  validatedForm: boolean;
  stepIndex: number;
  stepArrayText: any
  buttonNextSubmitText: any;

  constructor(private navCtrl: NavController, private translateService: TranslateService, private navParams: NavParams, private notificationService: NotificationsService, private modalCtrl: ModalController, public userData: UserData, private localStorage: LocalStorageService, private loading: LoadingService) {
    this.validatedForm = false;
    this.stepIndex = this.navParams.get('step') || 0;
    this.translateService.get('CUESTIONARIO_SALUD').subscribe((val) => {
      this.stepArrayText = [val.PASO1, val.PASO2, val.PASO3, val.PASO4];      
      this.buttonNextSubmitText = [val.SIGUIENTE, val.ENVIAR];
    });
    this.arrayForm = this.localStorage.getCuestionarioSalud() || [];
    this.form = this.arrayForm[this.stepIndex] || {};
    console.log("STEP INDEX: " + this.stepIndex);
  }


  ionViewDidLoad() {
    this.validateForm();
    //Se comprueba si la agenda está lista
    this.translateService.get('CUESTIONARIO_SALUD').subscribe((val) => {
      this.userData.getAppointments().subscribe(
        result => {
          if(result['finishedSchedule'] == 1 && !this.localStorage.getCuestionarioHideMessageAgenda()) {
            this.loading.showAlert(val.MSG_AGENDA);
            this.localStorage.setCuestionarioHideMessageAgenda(true);
          }
        },
        error => {}
      );
    });
  }
  

  nextSubmitForm() {
    this.arrayForm[this.stepIndex] = this.form;
    if(this.stepIndex !== this.stepArrayText.length - 1) {
      this.localStorage.setCuestionarioSalud(this.arrayForm);
      this.navCtrl.push(CuestionarioSaludPage, {step:this.stepIndex + 1});
    } else {
      let form = Object.assign(this.arrayForm[0],this.arrayForm[1],this.arrayForm[2],this.arrayForm[3]);
      this.buildFormToSend(form);
      //console.log(form);
      this.loading.show();
      this.userData.sendCuestionarioSalud(form).subscribe(
        (msg) => {
          this.loading.hide();
          this.localStorage.resetCuestionarioSalud();
          this.notificationService.cancelCuestionarioSalud();
          setTimeout(()=> {
            this.loading.showToast(msg);
            this.navCtrl.popToRoot(); 
          }, 300)
        },
        (error) => this.loading.showAlert(error)
      );
    }
  }

  validateForm() {
    if(this.stepIndex === 0)
      this.validatedForm = (this.form.weight && this.form.height && ((this.form.feeding === true) ? this.form.feedingDetail : true));
    if(this.stepIndex === 1)
      this.validatedForm = (((this.form.treatment === true) ? this.form.treatmentDetail : true) && (((this.form.disease ? this.form.disease.indexOf('otros') : -1) > -1) ? this.form.diseaseDetail : true) && ((this.form.surgery === true) ? this.form.surgeryDetail : true) && ((this.form.ache === true) ? this.form.acheDetail : true) && ((this.form.medicalAllergy === true) ? this.form.medicalAllergyDetail : true));
    if(this.stepIndex === 2)  
      this.validatedForm = (((this.form.foodAllergy === true)  ? this.form.foodAllergyDetail : true) && ((this.form.otherAllergy === true) ? this.form.otherAllergy : true));
    if(this.stepIndex === 3)  
      this.validatedForm = ((this.form.cleaning_period == 'Otro') ? this.form.cleaning_periodDetail : true) && ((this.form.coverage_period == 'Otro') ? this.form.coverage_periodDetail : true) && (this.form.acceptPolicy === true);
 }

  showTerms() {
    let modal = this.modalCtrl.create(PoliticaPrivacidadSaludPage);
    modal.present();
  }

  scrollToBottom() {
    this.content.scrollToBottom();
  }

  buildFormToSend (form) {
    Object.keys(form).forEach(key=> {
      if(typeof(form[key]) == 'boolean') {
        form[key] = (form[key] ? 1 : 0);
      }
      if(typeof(form[key]) == 'object') {
        form[key] = form[key].join(',');
      }
    })
  }

}

