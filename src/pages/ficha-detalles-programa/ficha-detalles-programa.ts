import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CACHED_IMAGES } from '../../models/models';

/*
  Generated class for the FichaDetallesProgramaPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-ficha-detalles-programa',
  templateUrl: 'ficha-detalles-programa.html'
})
export class FichaDetallesProgramaPage {

  item: any;
  treatments: any;
  cachedImages = CACHED_IMAGES;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.item = this.navParams.get('program');
    //this.treatments = Array.isArray(this.item.treatments) ? this.item.treatments : this.item.treatments.split(',');
    this.treatments = this.item.treatments;
  }

  ionViewDidLoad() {}

  getRandom() {
    return Date.now();
  }

}
