import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ActionSheetController, Platform } from 'ionic-angular';
import { TreatmentsData } from '../../providers/treatments-data';
import { LoadingService } from '../../providers/loading-service';
import { TreatmentsByCategory } from '../../models/models';
import { TratamientosPage } from '../../pages/tratamientos/tratamientos';
import { UserData } from '../../providers/user-data';
import { TranslateService } from 'ng2-translate';
import { CallNumber } from 'ionic-native';
import { SHA_EMAIL, SHA_TELEPHONE, SHA_WHATSAPP, CACHED_IMAGES } from '../../models/models';

@Component({
  selector: 'page-categorias-tratamientos',
  templateUrl: 'categorias-tratamientos.html'
})
export class CategoriasTratamientosPage implements OnInit{

  categories: Array<TreatmentsByCategory>;
  cachedImages = CACHED_IMAGES;
  addTreatmentDaySelected: any;

  constructor(public navCtrl: NavController, private navParams: NavParams, private treatmentsData: TreatmentsData, public userData: UserData, private loading: LoadingService,private translateService: TranslateService, private platform: Platform, private actionsheetCtrl: ActionSheetController) {
    this.addTreatmentDaySelected = this.navParams.get('day');
    let subcategories = this.navParams.get('subcategories');
    this.categories = subcategories ? subcategories : [];
  }

  ngOnInit() {
    if (this.categories.length == 0) {
      this.loading.show();
      this.treatmentsData.getTreatmentsByCategory().subscribe(
        result => {
          this.loading.hide();
          console.log(result)
          //Inserta los elementos de result(tipo Object) en el array categorias
          for (let i in result) {
            this.categories.push(result[i]);
          }
        },
        error => {
          this.loading.showAlert(error);
        }
      )
    }
  }

  ionViewDidLoad() {}

  categoryTapped(event, category) {

    if (category.treatments && category.treatments.length > 0){
      this.navCtrl.push(TratamientosPage, {
        treatments: category.treatments,
        day: this.addTreatmentDaySelected
      });
    } else if (category.subcategories && category.subcategories.length > 0) {
      this.navCtrl.push(CategoriasTratamientosPage, {
        subcategories:category.subcategories,
        day: this.addTreatmentDaySelected
      });
    } else {
      
    }

  }

  openActionSheet () {
    this.translateService.get('ACTION_SHEET_CONTACT').subscribe((val) => {
      let actionSheet = this.actionsheetCtrl.create({
        title: val.TITLE,
        cssClass: 'action-sheet',
        buttons: [
          {
            text: val.LLAMAR,
            icon: !this.platform.is('ios') ? 'call' : null,
            handler: () => {
              let tlfNumber = SHA_TELEPHONE.contactar || undefined;
              if(tlfNumber) {
                CallNumber.callNumber(tlfNumber, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch(() => window.open("tel:" + tlfNumber, '_system'));
              }
            }
          },
          {
            text: val.WHATSAPP,
            icon: !this.platform.is('ios') ? 'logo-whatsapp' : null,
            handler: () => {
              let tlfNumber = SHA_WHATSAPP.GR || undefined;
              if(tlfNumber) {
                window.open("whatsapp://send?phone=" + tlfNumber, '_system');
              }
            }
          },
          {
            text: val.ENVIAR_EMAIL,
            icon: !this.platform.is('ios') ? 'mail-open' : null,
            handler: () => {
              let email = SHA_EMAIL.contactar || undefined;
              if(email)
                window.open("mailto:" + email, '_system');
            }
          },
          {
            text: val.CANCELAR,
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel', // will always sort to be on the bottom
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    });
  }

  getRandom() {
    return Date.now();
  }
  

}
