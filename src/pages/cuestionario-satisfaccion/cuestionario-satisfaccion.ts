import { Component, ViewChild } from '@angular/core';
import { StatusBar } from 'ionic-native';
import { NavController, NavParams, Slides, Platform, AlertController, MenuController, Events, Keyboard } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { LoadingService } from '../../providers/loading-service';
import { TranslateService } from 'ng2-translate';
import { MiEstanciaPage } from '../mi-estancia/mi-estancia';

import { SERVER_SATISFACCION_ICONS, URL_TRIPADVISOR } from '../../models/models';

import moment from 'moment';


@Component({
  selector: 'page-cuestionario-satisfaccion',
  templateUrl: 'cuestionario-satisfaccion.html'
})
export class CuestionarioSatisfaccionPage {


  @ViewChild('Slides') slides: Slides;
  availableCuestionario:any = undefined;
  isSentCuestionario:any;
  sliderOptions:any;
  blocks: Array<any> = [];
  progressBarValue: any = 1;
  url_icons: string;
  form: any = [];
  negativeEvaluation: boolean = false;
  showComment: boolean = false;
  resultadoEncuesta: string = '';
  contactar:string = '';
  cuestionarioExpired:boolean = false;
  hideHeader:boolean = false;
  careTeam:Array<any> = [];
  isIOS:boolean = false;


  constructor(private navCtrl: NavController, public keyboard: Keyboard, public events:Events, public platform:Platform, private translateService: TranslateService, public menuCtrl: MenuController, public alertCtrl: AlertController ,private navParams: NavParams, public userData: UserData, private loading: LoadingService) {
    this.sliderOptions = {
      pager: false,
      onlyExternal: true
    }
    this.url_icons = SERVER_SATISFACCION_ICONS;

    platform.registerBackButtonAction(() => { console.log("backButtonPressed") });

  }


  ionViewDidEnter() {
    this.hideHeader = false;
    console.log("header " + this.hideHeader)
    this.loading.show()
    this.userData.getCuestionarioSatisfaccion().subscribe(
      result => {
        let data:any = result;
        console.log(data);
        this.availableCuestionario = parseInt(data.enabled);
        this.isSentCuestionario = parseInt(data.state);
        this.blocks = this.getVisibleBlocks(data.blocks);
        this._getCareTeam();
        this.loading.hide();
      },
      error => {
          this.loading.showAlert(error);
          this.navCtrl.setRoot(MiEstanciaPage);
          this.events.publish('menu:miEstancia');
      }
    );
    //console.log(moment().diff(moment(this.userData.user.checkOut,'DD-MM-YYYY'), 'days'))
    this.cuestionarioExpired = (moment().diff(moment(this.userData.user.checkOut,'DD-MM-YYYY'), 'days')) > 30;
  }


  getVisibleBlocks(blocks) {
    return blocks.filter(block => { return block.visible == 1 });
  }


  private _checkCommentOfBlock(block_id){
    let _block = this.blocks.filter(block => {return block.id == block_id})[0];
    let _questionsIds:Array<any> = _block.questions.map(element => { return element.id; });

    for (let questionId of _questionsIds) {
        if(this.form[questionId] < 7) {
          this.negativeEvaluation = true;
          this.showComment = true;
          break;
        } else {
          this.negativeEvaluation = false;
          this.showComment = false;
        }
    }
  }


  checkCommentOfBlock(ev, block_id, questionId){
    console.log(ev.value);
    this.form[questionId] = ev.value;
    this._checkCommentOfBlock(block_id);
  }


  private _updateCommentOfBlock(currentBlockId, direction) {
      setTimeout(()=>{
        let _blockIds = this.blocks.map(element => { return element.id; });
        let index = _blockIds.indexOf(currentBlockId);
        if(index >= 0 && index < _blockIds.length - 1) {
          let nextBlockId = _blockIds[index + direction]
          this._checkCommentOfBlock(nextBlockId);
        }
      },50);
  }


  private _prepareForm() {
    let _form:Array<any>,
        formToSend:Array<any> = [], 
        keys:Array<any> = [];

    _form = this.form;
    keys = Object.keys(_form);
    keys.map((key,index,array) => {
      if(array[index] && (_form[key] != undefined)) {
        let obj = {
                    blockId: this._getBlockOfQuestionId(key),
                    questionId: key,
                    value: _form[key]
                  }
        formToSend.push(obj);
      }
    })

    return formToSend;
  }


  private _getBlockOfQuestionId (id) {
    let block = this.blocks.filter(block => { 
        let result:boolean = false;
        block.questions.filter(q => { if(q.id == id) result = true })
        return result;
    })[0];

    return block.id;
  }



  private _getBlockById (id) {
    return this.blocks.filter(block => { return block.id == id })[0];
  }



  changeRadioButton(ev, blockId, questionId) {
    let _block = this.blocks.filter(block => { return block.id == blockId})[0];
    let _questions = _block.questions.filter(question => { return (question.dependencyId == questionId) && (question.dependencyIdEnableTo != ev)});
    //Resetea los valores de las preguntas asociadas a otra padre, cuando el usuario cambia de estado de la pregunta padre a un valor distinto a 'dependencyIdEnableTo'
    for(let q of _questions){
      this.form[q.id] = undefined;
    }
  }



  tappedCommentBtn() {
    this.showComment = !this.showComment;
  }


  next(currentBlockId) {
    let temp = this.blocks;
    this.blocks = [];
    this.blocks = temp;
    this._next();
    this._updateCommentOfBlock(currentBlockId, 1);  //1 next element
  }


  private _next() {
    //console.log("*****" + this.slides.getActiveIndex());
    if(this.slides.getActiveIndex() > 0) 
      this.hideHeader = true;
    this.slides.slideNext(250);
  }


  prev(currentBlockId) {
    //console.log("*****" + this.slides.getActiveIndex());
    this.slides.slidePrev(250);
    if(this.slides.getActiveIndex() > 1)   //para evitar que de error cuando se regresa a la página tutorial
      this._updateCommentOfBlock(currentBlockId, -1);  //-1 prev element
    else
      this.hideHeader = false;
  }


  save(){
    let formToSend = this._prepareForm();
    console.log(formToSend);
    this.loading.show();
    this.userData.setCuestionarioSatisfaccion(formToSend).subscribe(
      result => {
        let data:any = result;
        console.log(data);
        this.resultadoEncuesta = data.level;  //positivo, negativo, neutro
        setTimeout(()=>{
          this.loading.hide();
          this._next();          
        }, 500);
      },
      error => {
          this.loading.showAlert(error);
      }
    );
  }


  private _getCareTeam() {
    let _block = this._getBlockById(1);
    for(let q of _block.questions) {
      switch (q.id) {
        case '3':
          this.careTeam[q.id] = this.userData.user.careTeam.filter(elemen=>{ return elemen.role == 'GR' })[0];
          break;
        case '4':
          this.careTeam[q.id] = this.userData.user.careTeam.filter(elemen=>{ return elemen.role == 'GA' })[0];
          break;
        case '5':
          this.careTeam[q.id] = this.userData.user.careTeam.filter(elemen=>{ return elemen.role == 'Consultor nutricional' })[0];
          break;
        case '6':
          this.careTeam[q.id] = this.userData.user.careTeam.filter(elemen=>{ return elemen.role == 'Médico' })[0];
          break;   
        default:
          // code...
          break;
      }
    }
  }


  tappedCondiciones() {
    this.translateService.get('CUESTIONARIO_SATISFACCION').subscribe((val) => {
      let text = '<ul><li>' + val.CONDICION1 + '</li><li>' + val.CONDICION2 + '</li><li>' + val.CONDICION3 + '</li><li>' + val.CONDICION4 + '</li></ul><br><span>' + val.CONDICION5 + '</span>';
      this.loading.showAlert(text, val.CONDICIONES, 'alert_cuestionario');
    });   
  }


  tappedCerrar() {
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }


  tappedContactar() {
    this.loading.show();
    this.userData.setContactRequest(this.contactar).subscribe(
      result => {
          this.loading.hide();
          this.tappedCerrar();
      },
      error => {
        this.loading.showAlert(error);
      }
    );
  }


  tappedTripadvisor() {
    this.platform.ready().then(() => {
      window.open(URL_TRIPADVISOR,'_blank', 'location=yes,toolbar=yes')
    }); 
  }


  tappedMenu() {
    if(this.availableCuestionario && !this.isSentCuestionario) {
      this.translateService.get('CUESTIONARIO_SATISFACCION').subscribe((val) => {
        let alert = this.alertCtrl.create({
          title: val.ALERT_SKIP_TITLE,
          message: val.ALERT_SKIP_TEXT,
          buttons: [
            {
              text: val.CANCELAR,
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
                this.menuCtrl.close();
              }
            },
            {
              text: val.ACEPTAR,
              handler: () => {
                console.log('Aceptar clicked');
              }
            }
          ]
        });
        alert.present();
      })      
    }
  }


  

}


