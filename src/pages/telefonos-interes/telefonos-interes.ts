import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CallNumber} from 'ionic-native';
import { UserData } from '../../providers/user-data';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';



@Component({
  selector: 'page-telefonos-interes',
  templateUrl: 'telefonos-interes.html'
})
export class TelefonosInteresPage {

  items: any;

  constructor(public navCtrl: NavController, private http: Http, private userData: UserData) {
    if(!this.items) {
      this.http.get('assets/i18n/telephonesInterest.json').map(res => res.json())
        .subscribe(data => {
          this.items = (data[this.userData.user.lang].length == 0) ? data['es'] : data[this.userData.user.lang];
        }
      );
    }
  }

  ionViewDidLoad() {}

  telephoneTapped(phone_number) {
    CallNumber.callNumber(phone_number, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => window.open("tel:" + phone_number, '_system'));
  }

}
