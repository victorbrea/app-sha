import { Component, ViewChild, trigger, state, style, animate, transition } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { CalendarByWeekComponent } from '../../components/calendar-by-week/calendar-by-week';
import { UserData } from '../../providers/user-data';
import moment from 'moment';

@Component({
  selector: 'page-dieta',
  templateUrl: 'dieta.html',
  animations: [
    trigger('fadeInOut', [
      state('void', style({ opacity: '0' })),
      state('*', style({ opacity: '1' })),
      transition('void => *', animate('100ms ease-in'))
    ])
  ]
})
export class DietaPage {

  @ViewChild(CalendarByWeekComponent) calendar: CalendarByWeekComponent;
  stay: any;
  dailyDiet: any;
  moment: any;
  hideTodayButton:boolean;
  daySelected: string;
  noDailyDiet:any = false;

  constructor(public navCtrl: NavController, public userData: UserData, private navParams: NavParams, public platform: Platform) {
    this.stay = this.navParams.get('stay');
    this.moment = moment;
    this.daySelected = null;
    let today = moment();
    let inDate = moment(this.stay.checkIn, "DD/MM/YYYY").subtract(1, 'day')
    let outDate = moment(this.stay.checkOut, "DD/MM/YYYY").add(1, 'day')
    this.hideTodayButton = !today.isBetween(inDate, outDate);
  }

  ionViewDidLoad() {}

  dayChanged (day) {
    this.dailyDiet = undefined;
    //hack para crear el efecto fadeIn
    setTimeout(()=>{
      this.dailyDiet = this.stay.diet.filter((menu) => {return menu['day'] === day})[0];
      this.noDailyDiet = (!this.dailyDiet) ? true : false;
    },100)
    this.daySelected = day;
  }

  goToToday () {
    this.calendar.goToToday();
  }

}
