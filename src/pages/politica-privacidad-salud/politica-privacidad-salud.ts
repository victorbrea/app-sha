import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

/*
  Generated class for the TerminosCondicionesSalud page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-politica-privacidad-salud',
  templateUrl: 'politica-privacidad-salud.html'
})
export class PoliticaPrivacidadSaludPage {

  constructor(public navCtrl: NavController, private viewCtrl: ViewController) {}

  ionViewDidLoad() {
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
