import { Component, ViewChild, ElementRef, trigger, state, style, animate, transition } from '@angular/core';
import { NavController, NavParams, Events, ActionSheetController, Platform } from 'ionic-angular';
import { CalendarByWeekComponent } from '../../components/calendar-by-week/calendar-by-week';
import { UserData } from '../../providers/user-data';
import { LoadingService } from '../../providers/loading-service';
import { ComentarPage } from '../comentar/comentar';
import { CitaPage } from '../cita/cita';
import { CategoriasTratamientosPage } from '../categorias-tratamientos/categorias-tratamientos';
//import { MiLlegadaPage } from '../mi-llegada/mi-llegada';
import { CuestionarioSaludPage } from '../cuestionario-salud/cuestionario-salud';
import { CACHED_IMAGES } from '../../models/models';
import { SHA_EMAIL, SHA_LOCATION, SHA_TELEPHONE, SHA_WHATSAPP } from '../../models/models';
import { TranslateService } from 'ng2-translate';
import { LaunchNavigator, LaunchNavigatorOptions, CallNumber } from 'ionic-native';

import moment from 'moment';


@Component({
  selector: 'page-agenda',
  templateUrl: 'agenda.html',
  animations: [
    trigger('fadeInOut', [
      state('void', style({ opacity: '0' })),
      state('*', style({ opacity: '1' })),
      transition('void => *', animate('100ms ease-in'))
    ])
  ]
})

export class AgendaPage {

  @ViewChild(CalendarByWeekComponent) calendar: CalendarByWeekComponent;
  @ViewChild('headerTag') headerTag: ElementRef;
  @ViewChild('scrollableTag') scrollableTag: ElementRef;
  appointments: any;
  allAppointments: Array<any> = [];
  finishedSchedule: any;
  daySelected: string;
  isCuestionario: boolean = false;
  moment:any;
  noAppointments:any = false;
  cachedImages = CACHED_IMAGES;

  constructor(private navCtrl: NavController, public platform: Platform, private navParams: NavParams, private events: Events, public userData: UserData, private loading: LoadingService, public translateService: TranslateService, public actionsheetCtrl: ActionSheetController) {
    this.daySelected = null;
    this.moment = moment;
    //this.events.publish('menu:agenda');
  }

  ionViewDidLoad () {
  }

  ionViewWillEnter() {
    this.resize();
    if(this.navCtrl.last().component != CitaPage){
      this.userData.isSentCuestionarioSalud().subscribe(
        sendIt=> {this.isCuestionario = sendIt ? true : false; this._getAppointments()},
        error=> this._getAppointments()
      );
    }
  }

  _getAppointments() {
    this.loading.show();
    this.allAppointments = [];
    this.userData.getAppointments().subscribe(
        result => {
          this.loading.hide();

          this.allAppointments = result['appointments'];
          this.finishedSchedule = result['finishedSchedule'];

          console.log(result['appointments']);
                
          if(this.daySelected)
            this.dayChanged(this.daySelected);
        },
        error => {
          this.loading.showAlert(error);
        }
      )    
  }

  resize() {
    if (this.headerTag) {
      let offset = this.headerTag.nativeElement.offsetHeight;
      (<HTMLDivElement>this.scrollableTag.nativeElement).style.marginTop = offset + 'px';
    }
  }

  dayChanged (day) {
    this.appointments = undefined;  
    //hack para crear el efecto fadeIn
    let timeInterval = setInterval(()=>{
      if(this.allAppointments.length != 0){
        clearInterval(timeInterval);
        console.log(this.allAppointments);
        this.appointments = this.allAppointments.filter((appointment) => {return appointment['day'] == day});
        this.noAppointments = (this.appointments.length == 0) ? true : false;
      }
    }, 100);
    this.daySelected = day;
  }

  goToToday () {
    this.calendar.goToToday();
  }

  tappedAppointment (appointment) {
    this.navCtrl.push(CitaPage, {cita: appointment});
  }  

  comentar($event) {
    this.navCtrl.push(ComentarPage);
  }

  addTreatement($event) {
    this.navCtrl.push(CategoriasTratamientosPage, {day:this.daySelected});
  }

  cuestionarioTapped() {
    this.navCtrl.push(CuestionarioSaludPage);
  }

  openActionSheet () {
    this.translateService.get('ACTION_SHEET_CONTACT').subscribe((val) => {
      let actionSheet = this.actionsheetCtrl.create({
        title: val.TITLE,
        cssClass: 'action-sheet',
        buttons: [
          {
            text: val.LLAMAR,
            icon: !this.platform.is('ios') ? 'call' : null,
            handler: () => {
              let tlfNumber = SHA_TELEPHONE.default || undefined;
              if(tlfNumber) {
                CallNumber.callNumber(tlfNumber, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch(() => window.open("tel:" + tlfNumber, '_system'));
              }
            }
          },
          {
            text: val.ENVIAR_EMAIL,
            icon: !this.platform.is('ios') ? 'mail-open' : null,
            handler: () => {
              let email = SHA_EMAIL.reservas || undefined;
              if(email)
                window.open("mailto:" + email, '_system');
            }
          },
          {
            text: val.CANCELAR,
            icon: !this.platform.is('ios') ? 'close' : null,
            role: 'cancel', // will always sort to be on the bottom
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    });
  }

  getRandom() {
    return Date.now();
  }

}
