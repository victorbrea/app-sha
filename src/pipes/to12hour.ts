import { Injectable, Pipe } from '@angular/core';

import moment from 'moment';

@Pipe({
  name: 'to12hour'
})
@Injectable()
export class To12hour {
  transform(value) {

    let hour = moment(value, ['HH:mm']).format("hh:mm A");
    return hour;

  }
}
