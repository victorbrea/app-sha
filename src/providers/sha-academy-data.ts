import { Injectable } from '@angular/core';
import { RemoteStorageService } from '../providers/remote-storage-service';
import { Observable } from 'rxjs/Observable';

import { TranslateService } from 'ng2-translate';

import { UserData } from '../providers/user-data';

@Injectable()
export class ShaAcademyData {

  private activities: any;

  constructor(private remoteStorage: RemoteStorageService, private userData: UserData, private translateService: TranslateService) {}

  getAllActivities () {
    let params = {
      bookingId: this.userData.user.bookingId
    };
    return new Observable(observer => {
      if(!this.activities) {
        this.remoteStorage.connect('getShaAcademy', params).subscribe(
          (dt) => {
            let data = JSON.parse(dt);
            if (data.result == 1) {
              this.activities = data.message;
              observer.next(data.message);
              observer.complete();
            } else {
              this.translateService.get('ERRORES').subscribe((val) => {
                observer.error(val.SERVIDOR);
              });
            }
          },
          (error) => {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.CONEXION_SERVIDOR);
            });
          }
        );
      } else {
        observer.next(this.activities);
        observer.complete();
      }
    });
  }
  
}
