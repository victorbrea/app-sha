import { Injectable } from '@angular/core';
import { LocalNotifications } from 'ionic-native';
import { Events, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { LocalStorageService } from './local-storage-service';
import { WeatherLocationService } from './weather-location-service';
import { UserData } from './user-data';
import { LANGUAGE_DEFAULT } from '../models/models';


import moment from 'moment';


@Injectable()
export class NotificationsService{

  private notificationsTemplate;
  badge : number;
  notificationsToShow: Array<any>;

  constructor(private platform: Platform, private http: Http, private userData: UserData, private localStorage: LocalStorageService, private weather: WeatherLocationService, private events: Events) {
    this.notificationsToShow = this.localStorage.getNotificationsToShow() || [];
    this.badge = 0;
    this.notificationsTemplate = [];
  }

  showNotification (notification) {
    notification.data.viewed = false;
    let array = this.notificationsToShow.filter(elem => {return elem.id != notification.id})
    array.unshift(notification);
    this.notificationsToShow = array;
    this.saveNotificationsToShow();
    this.updateBadge();
  }

  hideNotification (notification) {
    let array = this.notificationsToShow.filter(elem => {return elem.id != notification.id});
    this.notificationsToShow = array;
    this.saveNotificationsToShow();
    this.updateBadge();
  }

  cancelAllNotifications () {
    this.notificationsToShow = [];
    LocalNotifications.cancelAll();
  }

  private saveNotificationsToShow () {
   this.localStorage.setNotificationsToShow(this.notificationsToShow);
  }

  private updateBadge () {
    let arrElem = this.notificationsToShow.filter(elem => {
      return (elem.data.viewed === false);
    });
    this.badge = Number(arrElem.length);
    this.events.publish('menu:badge'); 
  }

  //recibe como parámetro el id o el tipo de notificación
  getNotificationTemplate (param) {
    return new Promise(resolve => {
      if (this.notificationsTemplate.length == 0) {
        this.http.get('assets/i18n/notificationsTemplate.json').map(res => res.json()).subscribe(data => 
          {
            this.notificationsTemplate = (data[this.userData.user.lang].length == 0) ? data[LANGUAGE_DEFAULT] : data[this.userData.user.lang];
            let array = this.notificationsTemplate.filter(elem => (param == elem.id) || (param == elem.data.type));
            if (param == 'consejo')
              resolve(array);
            else
              resolve(array[0]);
          }
        );
      } else {
        let array = this.notificationsTemplate.filter(elem => (param == elem.id) || (param == elem.data.type));
        if (param == 'consejo')
          resolve(array);
        else
          resolve(array[0]);
      }
    });
  }

  markViewedAllNotificationsToShow() {
    this.notificationsToShow.map(elem => {elem.data.viewed = true})
    this.saveNotificationsToShow();
    this.updateBadge();
    this.notificationsToShow.filter(elem => {return elem.data.type == 'citaReminder'})
      .map(elem=> {
        this.hideNotification(elem);
      });
  }

  markViewedNotificationsToShow(id) {
    this.notificationsToShow.filter(elem => {return elem.id == id}).map(elem=> {
      elem.data.viewed = true;
    });
    this.saveNotificationsToShow();
    this.updateBadge();
  }

  scheduleAllNotifications () {
    //Notificacion de meteo
    if(this.userData.user.fase != 'postSha') {
      this.initMeteoNotification();
    }
    //Resto notificaciones locales
    if (this.localStorage.getStateScheduleNotification() != "yes") {
      if(this.userData.user.fase == 'preSha') {
        //cuestionarioSaludNotification
        if(!this.localStorage.getCuestionarioSalud()) {
          this.userData.isSentCuestionarioSalud().subscribe(sendIt=> {if (!sendIt) this.scheduleCuestionarioSalud()});
        }
        //consejos
        this.scheduleConsejos();
        //configurar salida
        this.userData.getDeparture().subscribe(form => {if (!form['configured']) this.scheduleConfigurarSalida()});
      } else if (this.userData.user.fase == 'Sha') {
        //this.cancelCuestionarioSalud ();
        this.scheduleTratamientoBelleza();
        this.userData.getDeparture().subscribe(form => {if (!form['configured']) this.scheduleConfigurarSalida()});
      } else {
        this.notificationsToShow = [];
      }
      this.localStorage.setStateScheduleNotification("yes");
    }
  }

  scheduleNotification (notification) {
    let conf = {
      id: notification.id,
      title: notification.title,
      text: notification.text,
      at: notification.at,
      data: notification.data || '',
      icon: "res://icon.png",
      smallIcon: "res://ic_stat_notify.png",
      led: '498ec7'
    }
    if (notification.every == 'day')
      conf['every'] = 'day';

    //crea la notificacion
    LocalNotifications.schedule(conf);
  }

  eventNotificationsHandles () {
    LocalNotifications.on("trigger", (notifElemen, state) => {
      let notification = notifElemen;
      notification.data = JSON.parse(notification.data);
      if(notification.data.type != "citaReminder")
        this.showNotification(notification);
    });
  }

//***** Notificación Meteo *****

  private initMeteoNotification () {
    this.getNotificationTemplate('meteo').then(template => {
      let _notification = this.notificationsToShow.filter(elem => {return elem.id === template['id']})[0];
      _notification = (_notification) ? _notification : template;
      let nowDate = moment().format("DD/MM/YYYY");
      if(_notification.data.lastDayViewed !== nowDate) {
        this.weather.getWeatherByFaseUser(this.userData.user.fase).then(data => {
          if (data == null) {
            this.hideNotification(_notification)
          } else {
            _notification.data.viewed = false;
            _notification.data.lastDayViewed = nowDate;
            _notification.data.content = data;
            if(data['day'])
              this.showNotification(_notification)
            else
              this.hideNotification(_notification)
          }
        })
      }
    })
  }

//***** Notificacion Cuestionario Salud *****

  scheduleCuestionarioSalud () {
    this.getNotificationTemplate('cuestionarioSalud').then(template => {
      let notification = template;
      if(this.platform.is('cordova')) {
        LocalNotifications.isScheduled(notification['id']).then(scheduled => {
          if(!scheduled) {
            notification['at'] = moment("11:00", "HH:mm").add(1,'day').toDate(); //el día siguiente a las 11:00
            this.scheduleNotification(notification)
          }
        })
      }
    })
  }


  cancelCuestionarioSalud () {
    this.getNotificationTemplate('cuestionarioSalud').then(template => {
      let notification = template;
      if(this.platform.is('cordova')) {
        LocalNotifications.isScheduled(notification['id']).then(scheduled => {
          LocalNotifications.cancel(notification['id']);      
        });
        this.hideNotification(notification)
      }
    })
  }

//***** Notificacion Confirmar Llegada *****

  scheduleConfirmMiLlegadaNotification (checkIn, hourIn) {
    this.getNotificationTemplate('confirmacionLlegada').then(template => {
      let notification = template;
      if(hourIn) {
        let date = moment(checkIn + " " + hourIn, "DD/MM/YYYY HH:mm").subtract(3, 'hours');
        if(moment().isBefore(date)) {
          if(this.platform.is('cordova')) {
            notification['at'] = date.toDate();
            notification['title'] = notification['title'] + " " + moment(hourIn, "HH:mm").format("HH:mm A");
            notification['data']['title'] = notification['data']['title'] + " " + moment(hourIn, "HH:mm").format("HH:mm A");
            this.scheduleNotification(notification);
          }
        }
      }
    })
  }

  cancelConfirmMiLlegadaNotification () {
    this.getNotificationTemplate('confirmacionLlegada').then(template => {
      let notification = template;
      if(this.platform.is('cordova')) {
        LocalNotifications.isScheduled(notification['id']).then(scheduled => {
          LocalNotifications.cancel(notification['id']);      
        });
        this.hideNotification(notification)
      }
    })
  }

//***** Notificación Cita Reminder *****

  scheduleCitaReminder (cita, at) {
    this.getNotificationTemplate('citaReminder').then(template => {
      let notification = (JSON.parse(JSON.stringify(template))) // crea un objeto nuevo en vez de asignarse por referencia
      notification['id'] = at.clone().unix();
      if(this.platform.is('cordova')) {
         LocalNotifications.isScheduled(notification['id']).then(scheduled => {
          if(!scheduled) {
            notification['title'] = cita.name;
            notification['text'] = (this.localStorage.getLang() != 'es') ? ("Today at " + cita.start + " in " + cita.room) : ("Hoy a las " + cita.start + " en " + cita.room);
            notification['at'] = at.toDate();
            notification['data']['params'] = cita;
            console.log(notification);
            this.scheduleNotification(notification);
          }
        })
      }
    })
  }

  isScheduledCitaReminder (id) {
    return new Promise(resolve=> {
      if(this.platform.is('cordova')) {
        LocalNotifications.isScheduled(id).then(scheduled => {
          resolve(scheduled);
        })
      } else
        resolve(false);
    })
  }

  cancelReminderCita (id) {
    if(this.platform.is('cordova')) {
      LocalNotifications.isScheduled(id).then(scheduled => {
        LocalNotifications.cancel(id);     
      });
    }
  }


//***** Notificacion Consejos *****
  
  scheduleConsejos() {
    this.getNotificationTemplate('consejo').then(template => {
      let arrayNotifications = template;
      let checkIn = moment(this.userData.user.checkIn, 'DD/MM/YYYY').hour(0).minute(0).second(0).millisecond(0);
      let now = moment().hour(0).minute(0).second(0).millisecond(0);
      let numConsejoNotifications = 7; //numero de notificaciones del tipo 'consejo'
      let date, arrayNotScheduled = [];
      for (let i = numConsejoNotifications - 1; 0 <= i; --i) {
        date = checkIn.clone().subtract(numConsejoNotifications - i, 'days');
        if (date.clone().diff(now, 'days') > 0) {
          this.scheduleConsejo(arrayNotifications[i], date)
        } else {
          arrayNotScheduled.unshift(arrayNotifications[i]);
        }
      }
      if (arrayNotScheduled.length != 0) {
        for(let notification of arrayNotScheduled) {
          this.showNotification(notification);
        }
      }
    })
  }

  private scheduleConsejo(notification, at) {
    console.log(notification);
    if(this.platform.is('cordova')) {
      LocalNotifications.isScheduled(notification['id']).then(scheduled => {
        if(!scheduled) {
          notification['at'] = at.clone().hour(11).minute(0).toDate();
          this.scheduleNotification(notification);
        }
      })
    }
  }


//***** Notificación Configurar Mi Salida *****

  scheduleConfigurarSalida() {
    this.getNotificationTemplate('configurarSalida').then(template => {
      let notification = template;
      if(this.platform.is('cordova')) {
        LocalNotifications.isScheduled(notification['id']).then(scheduled => {
          if(!scheduled) {
            // 2 días antes del checkout a las 11:00 am
            let date = moment(this.userData.user.checkOut + " 11:00", "DD/MM/YYYY HH:mm").subtract(2, 'days');
            notification['at'] = date.toDate();
            notification['data']['title'] = moment().locale(this.userData.user.lang).format("dddd [,] DD MMMM");
            this.scheduleNotification(notification)
          }
        })
      }
    })    
  }

  cancelConfigurarSalidaNotification () {
    this.getNotificationTemplate('configurarSalida').then(template => {
      let notification = template;
      if(this.platform.is('cordova')) {
        LocalNotifications.isScheduled(notification['id']).then(scheduled => {
          LocalNotifications.cancel(notification['id']);      
        });
        this.hideNotification(notification)
      }
    })
  }


//***** Notificación Tratamiento Belleza*****

  scheduleTratamientoBelleza() {
    this.getNotificationTemplate('tratamientoBelleza').then(template => {
      let notification = template;
      if(this.platform.is('cordova')) {
        LocalNotifications.isScheduled(notification['id']).then(scheduled => {
          if(!scheduled) {
            // 2 días antes del checkout a las 11:00 am
            let date = moment(this.userData.user.checkOut + " 12:00", "DD/MM/YYYY HH:mm").subtract(2, 'days');
            notification['at'] = date.toDate();
            notification['data']['title'] = moment().locale(this.userData.user.lang).format("dddd [,] DD MMMM");
            this.scheduleNotification(notification);
          }
        })
      }
    })    
  }

//***** Notificacion Agenda Lista *****

//***** Notificacion Newsletter *****

}
