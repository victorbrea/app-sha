import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LocalStorageService {

  constructor(private http: Http) {}

  setLang (data) {
    localStorage.setItem('lang', data); 
  }

  setUserLogged (data) {
    localStorage.setItem('userLoggedId', data); 
  }

  setWeekStartDay (data) {
    localStorage.setItem('weekStartDay', data); 
  }

  setCuestionarioSalud (data) {
    localStorage.setItem('cuestionarioSalud', JSON.stringify(data)); 
  }

  setNotificationsToShow (data) {
    localStorage.setItem('notificationsToShow', JSON.stringify(data)); 
  }

  setOnBoarding (data) {
    localStorage.setItem('onBoarding', data);
  }

  setAdviceTime (data) {
    localStorage.setItem('adviceTime', data);
  }

  setStateScheduleNotification (data) {
    localStorage.setItem('stateScheduleNotifications', data);
  }

  setCuestionarioHideMessageAgenda (data) {
    localStorage.setItem('cuestionarioHideMessageAgenda', data);
  }

  getLang () {
    return localStorage.getItem('lang'); 
  }

  getUserLogged () {
    return localStorage.getItem('userLoggedId'); 
  }

  getWeekStartDay () {
    return localStorage.getItem('weekStartDay'); 
  }

  getCuestionarioSalud () {
    return JSON.parse(localStorage.getItem('cuestionarioSalud')); 
  }

  getNotificationsToShow () {
    return JSON.parse(localStorage.getItem('notificationsToShow'));
  }

  getAdviceTime () {
    return localStorage.getItem('adviceTime'); 
  }

  getOnBoarding () {
    return localStorage.getItem('onBoarding');
  }

  getStateScheduleNotification () {
    return localStorage.getItem('stateScheduleNotifications');
  }

  getCuestionarioHideMessageAgenda () {
    return localStorage.getItem('cuestionarioHideMessageAgenda');
  }

  resetCuestionarioSalud () {
    localStorage.removeItem('cuestionarioSalud');
  }

  resetLocalStorage () {
    localStorage.removeItem('userLoggedId');
    localStorage.removeItem('cuestionarioSalud');
    localStorage.removeItem('sentCuestionarioSalud');
    localStorage.removeItem('cuestionarioSaludIsSent');
    localStorage.removeItem('notificationsToShow');
    localStorage.removeItem('stateScheduleNotifications');
    localStorage.removeItem('onBoarding');
    localStorage.removeItem('cuestionarioHideMessageAgenda');
    this.setAdviceTime('15-minutes');
    this.setWeekStartDay(1);
  }

}
