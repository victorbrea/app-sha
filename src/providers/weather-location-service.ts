import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { URL_API_TIEMPO, URL_API_IMAGES_TIEMPO } from '../models/models';

import { UserData } from './user-data';

import moment from 'moment';
import xml2js from 'xml2js';


@Injectable()
export class WeatherLocationService {

  constructor(public http: Http, private userData: UserData) {}

  //obtiene un array de predicción meteorológica de los 7 días siguientes al actual 
  private getWeatherPredictions () {
    return new Promise((resolve) => {
      this.http.get(URL_API_TIEMPO).subscribe((result) => {
        xml2js.parseString(result['_body'], (err, data) => {
          if (!err) {
            //console.log(data.root.prediccion[0].dia);
            resolve(data.root.prediccion[0].dia);
          } else {
            resolve(null);
          }
        })
      })
    });    
  }

  //obtiene la predicción meteorológica del día: 'day' con formato YYYY-MM-DD
  private getWeatherOfDay(day) {
    return new Promise((resolve) => {
      let weatherObj = {}
      let skyDescription;
      let skyImg;
      this.getWeatherPredictions().then((dias) => {
        if(dias) {
          for (let i in dias) {
            if (dias[i].$.fecha === day) {
              //bucle para escoger la primera franga horaria que contenga descripcion e imagen del cielo
              for (let c in dias[i].estado_cielo) {
                if(dias[i].estado_cielo[c].$.descripcion !== "") {
                  skyDescription = dias[i].estado_cielo[c].$.descripcion;
                  skyImg = URL_API_IMAGES_TIEMPO + dias[i].estado_cielo[c]._ + "_g.png"
                  break;
                }
              }
              weatherObj = {
                tempMax: dias[i].temperatura[0].maxima[0],
                tempMin: dias[i].temperatura[0].minima[0],
                sky: skyDescription,
                skyImage: skyImg,
                day: moment(dias[i].$.fecha,'YYYY-MM-DD').locale(this.userData.user.lang).format('LLLL').split(' 0:00')[0]
              }
            }
          }
        }
        resolve(weatherObj);
      })
    });
  }

  getWeatherByFaseUser(fase) {
    if(fase === 'preSha')
      return this.getWeatherCheckInDay();
    else if(fase === 'Sha')
      return this.getWeatherToday();
    else
      return null;
  }

  //Predicción meteorológica del día de llegada
  getWeatherCheckInDay() {
    let checkInDay = moment(this.userData.user.checkIn,'DD/MM/YYYY').format('YYYY-MM-DD');
    return new Promise((resolve) => {
      this.getWeatherOfDay(checkInDay).then((data) => resolve(data));
    });
  }

  //Predicción meteorológica de hoy
  getWeatherToday() {
    let today = moment().format('YYYY-MM-DD');
    return new Promise((resolve) => {
      this.getWeatherOfDay(today).then((data) => resolve(data));
    });    
  }

  //Predicción meteorológica de mañana
  getWeatherTomorrow() {
    let tomorrow = moment().add(1, 'day').format('YYYY-MM-DD');
    return new Promise((resolve) => {
      this.getWeatherOfDay(tomorrow).then((data) => resolve(data));
    });    
  }
}