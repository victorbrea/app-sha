import { Injectable } from '@angular/core';
import { LocalStorageService } from './local-storage-service';
import { RemoteStorageService } from './remote-storage-service';
import { TranslateService } from 'ng2-translate';
import { Platform, Events } from 'ionic-angular';
import { User, LANGUAGES, SHA_EMAIL} from '../models/models';
import { Observable } from 'rxjs/Observable';



@Injectable()
export class UserData{

  user: User;

  constructor(private events: Events, private platform: Platform, private translateService: TranslateService, private localStorage: LocalStorageService, private remoteStorage: RemoteStorageService) {   
    this.user = { id: null, name: null, image: null, email: null, email_medical: null, phone: null, checkIn: null, checkOut: null, fase: null, preferences: null, programs: null, suite: null, careTeam: [], additionalTreatments: [], prescriptions: [], stays: [], diet: [], shafire: null, lang: null, weekStartDay: null, adviceTime: null, bookingId: null, appointments: [] };
    this.initWeekStartDay();
    this.initAdviceTime();
  }

  private setUser(user) {
    //Asigna los datos de usuario procedentes del servidor
    this.user.id = user.id;
    this.user.name = user.name;
    this.user.image = user.image;
    this.user.email = user.email;
    this.user.email_medical = user.email_medical;
    this.user.phone = user.phone;
    this.user.fase = user.fase;
    this.user.programs = user.programs;
    this.user.preferences = user.preferences;
    this.user.suite = user.suite;
    this.user.careTeam = user.careTeam;
    this.user.prescriptions = user.prescriptions;
    this.user.stays = user.stays;
    this.user.diet = user.diet;
    this.user.shafire = user.shafire;
    this.user.checkIn = user.checkIn;
    this.user.checkOut = user.checkOut;
    this.user.bookingId = user.bookingId;
    this.user.appointments = user.appointments;
    this.user.additionalTreatments = this._getAdditionalTreatments();

    //lanza el evento para capturar en 'app.components.ts'(módulo padre) el nombre del usuario
    //setTimeout(()=> this.events.publish('user:name', this.user.name), 200); //da tiempo a que se cargue el menu
    
  }

  public login(credentials) {
    return new Observable(observer => {
      let params = {
        email:credentials.email,
        password:credentials.password
      };
      this.remoteStorage.connect('login', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            this.localStorage.setUserLogged(data.message.id);
            this.setUser(data.message);
            observer.next(null);
            observer.complete();
          } else if(data.result == 0) {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.next(val.LOGIN_FALLIDO);
              observer.complete();
            });
          } else {
            //Fallo de servidor -1
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }

  public logout() {
    return new Observable(observer => { 
      this.localStorage.resetLocalStorage();
      observer.next(true);
      observer.complete();
    });
  } 

  public load(idUser) {
    return new Observable(observer => {
      let params = {
        id: idUser
      };
      this.remoteStorage.connect('getUser', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            this.setUser(data.message);
            console.log(data.message)
            observer.next();
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }


  public getAppointments() {
    return new Observable(observer => {
      let params = {
        bookingId: this.user.bookingId,
        userId: this.user.id
      };
      this.remoteStorage.connect('getAppointments', params).subscribe(
        (dt) => {
          //console.log(dt)
          let data = JSON.parse(dt);
          if (data.result == 1) {
            observer.next(data.message);
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }


  public getFase() {
    return new Observable(observer => {
      let params = {
        bookingId: this.user.bookingId,
        userId: this.user.id
      };
      this.remoteStorage.connect('getFase', params).subscribe(
        (dt) => {
          //console.log(dt)
          let data = JSON.parse(dt);
          if (data.result == 1) {
            observer.next(data.message.fase);
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          console.log(error)
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(error);
          });
        }
      )
    });
  }



  public recoveryPassword(email) {
    return new Observable(observer => {       
      let params = {
        id: this.user.id,
        email: email
      }
      this.remoteStorage.connect('recoveryPassword', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            this.translateService.get('RECUP_PASSWD.SUCCESS_MSG', {value: email}).subscribe((val1) => {
                this.translateService.get('RECUP_PASSWD.SUCCESS_MSG_TITLE').subscribe((val2) => {
                observer.next({title:val2, subtitle:val1});
                observer.complete();
              }); 
            });
          } else if(data.result == 0) {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.RECUP_FALLIDO);
            });
          } else {
            //Fallo de servidor -1
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }

  public changePassword(passwd_actual, passwd_new) {
    let params = {
      id: this.user.id,
      email: this.user.email,
      password: passwd_new,
      password_actual: passwd_actual
    }
    return new Observable(observer => {
      this.remoteStorage.connect('changePassword', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            observer.next(true);
            observer.complete();
          } else {
            observer.next(false);
            observer.complete();
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }

  public sendCuestionarioSalud(dataObj) {
    let params = {
      userId: this.user.id,
      bookingId: this.user.bookingId,
      form: dataObj
    }
    console.log(params);
    return new Observable(observer => {
      this.remoteStorage.connect('setCuestionarioSalud', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            this.translateService.get('COMENTAR').subscribe((val) => {
              observer.next("Cuestionario recibido correctamente");
              observer.complete();
            });
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }


  public isSentCuestionarioSalud() {
    let params = {
      userId: this.user.id,
      bookingId: this.user.bookingId,
    }
    console.log(params);
    return new Observable(observer => {
      this.remoteStorage.connect('getCuestionarioSalud', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            let status = (data.message.status == 1) ? true : false;
            observer.next(status);
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }


  public getCuestionarioSatisfaccion() {
    let params = {
      userId: this.user.id,
      bookingId: this.user.bookingId
    }

    return new Observable(observer => {
      this.remoteStorage.connect('getSurvey', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
              observer.next(data.message);
              observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }


  public setCuestionarioSatisfaccion(form) {
    let params = {
      userId: this.user.id,
      bookingId: this.user.bookingId,
      params: form
    }

    return new Observable(observer => {
      this.remoteStorage.connect('setSurvey', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
              observer.next(data.message);
              observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }


  public setContactRequest(form) {
    let params = {
      userId: this.user.id,
      bookingId: this.user.bookingId,
      value: form
    }

    return new Observable(observer => {
      this.remoteStorage.connect('setContactRequest', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
              observer.next(data.message);
              observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }



  public sendComment(dataObj) {
    let params = dataObj;
    params.userId = this.user.id;
    params.bookingId = this.user.bookingId;
    params.shaNotificationemail = SHA_EMAIL.reservas || '';

    return new Observable(observer => {
      this.remoteStorage.connect('setComment', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            this.translateService.get('COMENTAR').subscribe((val) => {
              observer.next(val.COMENTARIO_ENVIADO);
              observer.complete();;
            });
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }

  public confirmArrival(value) {
    let params = { 
      userId: this.user.id,
      userName: this.user.name,
      bookingId: this.user.bookingId,
      shaNotificationEmail: SHA_EMAIL.reservas,
      shaGuestOfficerEmail: '',
      value: (value == 1) ? "Llegará puntual" : "Llegará con retraso"
    }
    return new Observable(observer => {
      this.remoteStorage.connect('confirmArrival', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            this.translateService.get('CONF_LLEGADA').subscribe((val) => {
              observer.next(val.SUCCESS_MSG);
              observer.complete();
            });
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }

  public sendArrival(dataObj) {
    let params = dataObj;
    params.userId = this.user.id;
    params.bookingId = this.user.bookingId;
    return new Observable(observer => {
      this.remoteStorage.connect('setArrival', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            this.translateService.get('CONF_LLEGADA').subscribe((val) => {
              observer.next(val.SUCCESS_MSG);
              observer.complete();
            });
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });   
  }

  public getArrival() {
    let params = {
      userId: this.user.id,
      bookingId: this.user.bookingId
    }
    return new Observable(observer => {
      this.remoteStorage.connect('getArrival', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            observer.next(data.message);
            observer.complete();
          } else if(data.result == 0) {
            observer.next(null);
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });   
  }

  public sendDeparture(dataObj) {
    let params = dataObj;
    params.userId = this.user.id;
    params.bookingId = this.user.bookingId;
    return new Observable(observer => {
      this.remoteStorage.connect('setDeparture', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            this.translateService.get('CONF_SALIDA').subscribe((val) => {
              observer.next(val.SUCCESS_MSG);
              observer.complete();
            });
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });   
  }

  public getDeparture() {
    let params = {
      userId: this.user.id,
      bookingId: this.user.bookingId
    }
    return new Observable(observer => {
      this.remoteStorage.connect('getDeparture', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            observer.next(data.message);
            observer.complete();
          } else if(data.result == 0) {
            observer.next(null);
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });   
  }

  public uploadUserImage(image) {
    let params = {
      userId: this.user.id,
      imgData: image
    }
    return new Observable(observer => {
      this.remoteStorage.connect('uploadUserImage', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            observer.next(data.message.url);
            //alert(JSON.stringify(data.message));
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }


  public requestInvoice(dataObj) {
    let params = dataObj;
    console.log(params);
    return new Observable(observer => {
      this.remoteStorage.connect('requestInvoice', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            this.translateService.get('ESTANCIA').subscribe((val) => {
              observer.next(val.SOLICITUD_ENVIADA);
              observer.complete();;
            });
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }


  public getTransferPrice(form) {
    let params = form;
    console.log(params)
    return new Observable(observer => {
      this.remoteStorage.connect('getTransferPrice', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            observer.next(data.message.transferPrice);
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }

  private _getAdditionalTreatments() {

    let _treatments = [];
    if (this.user.appointments.length != 0)
      _treatments = this.user.appointments.filter(appointment => {return ((appointment['type'] == 'additional') && (appointment['hidden'] != 1))}).map(item =>{return item['name']});
    console.log(this.user.appointments);
    return _treatments;
  }


//**** Acceso a la base de datos local del dispositivo ****

  public setLang(lang: string) {
    if (LANGUAGES.indexOf(lang) > -1) {
      this.user.lang = lang;
      this.localStorage.setLang(lang);  
    }
  }

  public setWeekStartDay(numDay: number) {
    if (numDay == 0 || numDay == 1) {
      this.user.weekStartDay = numDay;
      this.localStorage.setWeekStartDay(numDay.toString()); 
    }
  }

  private initWeekStartDay() {
    let weekDay = this.localStorage.getWeekStartDay();
    this.setWeekStartDay(parseInt(weekDay || '1'))
  }

  public setAdviceTime(time: string) {
    this.user.adviceTime = time;
    this.localStorage.setAdviceTime(time); 
  }

  private initAdviceTime() {
    let adviceTime = this.localStorage.getAdviceTime() || '15-minutes';
    this.setAdviceTime(adviceTime);   
  }


//********************************************************** 

}
