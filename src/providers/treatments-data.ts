import { Injectable } from '@angular/core';
import { RemoteStorageService } from '../providers/remote-storage-service';
import { Observable } from 'rxjs/Observable';

import { TranslateService } from 'ng2-translate';


@Injectable()
export class TreatmentsData {

  constructor(private remoteStorage: RemoteStorageService, private translateService: TranslateService) {}

  getTreatmentsByCategory() {
    let params = {};
    return new Observable(observer => {
      this.remoteStorage.connect('getTreatmentsByCategory', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            observer.next(data.message);
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }

  getBeautyTreatments() {
    let params = {};
    return new Observable(observer => {
      this.remoteStorage.connect('getBeautyTreatments', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            observer.next(data.message);
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }

  getTreatmentAvailability(userId, treatmentId, bookingId, day) {
    let params = {
      userId: userId,
      treatmentId: treatmentId,
      bookingId: bookingId,
      day:day
    };
    //console.log(userId, treatmentId, bookingId)
    return new Observable(observer => {
      this.remoteStorage.connect('getTreatmentAvailability', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            observer.next(data.message);
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }

  scheduleTreatment(bookingId, userId, treatmentId, day, start, end, price, doctorId) {
    let params = {
      bookingId: bookingId,
      userId: userId,
      treatmentId: treatmentId,
      day: day,
      start: start,
      end: end,
      price: price,
      doctor: doctorId
    }
    //console.log(params);
    return new Observable(observer => {
      this.remoteStorage.connect('scheduleTreatment', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            this.translateService.get('TRATAMIENTO').subscribe((val) => {
              observer.next(val.SUCCESS_MSG);
            });
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }

}
