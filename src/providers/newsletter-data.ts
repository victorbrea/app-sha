import { Injectable } from '@angular/core';
import { RemoteStorageService } from '../providers/remote-storage-service';
import { Observable } from 'rxjs/Observable';
import { UserData } from '../providers/user-data';

import { TranslateService } from 'ng2-translate';

@Injectable()
export class NewsletterData {

  private newsletter: any;

  constructor(private remoteStorage: RemoteStorageService, private userData: UserData, private translateService: TranslateService) {}

  getNewsletter () {
    let params = {
      bookingId: this.userData.user.bookingId
    };
    return new Observable(observer => {
      if(!this.newsletter) {
        this.remoteStorage.connect('getNewsletter', params).subscribe(
          (dt) => {
            let data = JSON.parse(dt);
            if (data.result == 1) {
              this.newsletter = data.message;
              console.log(this.newsletter);
              observer.next(data.message);
              observer.complete();
            } else {
              this.translateService.get('ERRORES').subscribe((val) => {
                observer.error(val.SERVIDOR);
              });
            }
          },
          (error) => {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.CONEXION_SERVIDOR);
            });
          }
        );
      } else {
        observer.next(this.newsletter);
        observer.complete();
      }
    });
  }
  
}