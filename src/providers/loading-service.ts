import { Injectable } from '@angular/core';
import { LoadingController, Loading, AlertController, ToastController } from 'ionic-angular';
import { Keyboard } from 'ionic-native';
import { TranslateService } from 'ng2-translate';


@Injectable()
export class LoadingService {

  loading: Loading;
  loadingActive: boolean;
  espere: string;


  constructor(public loadingCtrl: LoadingController, public alertCtrl: AlertController, public toastCtrl: ToastController, public translateService: TranslateService) {
    this.loadingActive = false;
    this.changeLanguage();
  }

  public changeLanguage() {
    this.translateService.get('LOADING').subscribe((val) => {
      this.espere = val.ESPERE;
    });    
  }


  public show(msg: string = this.espere) {
    this.translateService.get('LOADING').subscribe((val) => {
      let message = msg || val.ESPERE;
      this.loading = this.loadingCtrl.create({
        content: message
      });
      if(!this.loadingActive) {
        this.loading.present();
        this.loadingActive = true;
      }
    });
  }

  public hide() {
    if(this.loadingActive) {
      this.loading.dismiss();
      this.loadingActive = false;
    }
  }

  public showAlert(subtitle, title = '', clase= '', time= 600) {
    this.translateService.get('LOADING').subscribe((val) => {
      let aceptar = val.ACEPTAR;
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subtitle,
        buttons: [aceptar],
        cssClass: clase
      });
      Keyboard.close()
      this.hide(); 
      setTimeout(() => {
        alert.present();
      },time);
    }); 
  }

  public showToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top',
      showCloseButton: false,
      closeButtonText: 'Ok'
    });
    Keyboard.close()
    this.hide(); 
    setTimeout(() => {
      toast.present();
    },600);
  }

  public showAlertRetry(subtitle, title = '') {
    this.translateService.get('LOADING').subscribe((val) => {
      let reintentar = val.REINTENTAR;
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subtitle,
        buttons: [reintentar]
      });
      Keyboard.close()
      this.hide(); 
      setTimeout(() => {
        alert.present();
      },600);
    });     
  }

}
