import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { LocalStorageService } from './local-storage-service';

import { SERVER } from '../models/models';

@Injectable()
export class RemoteStorageService {

  private urlDataServer: any = SERVER;
  private headers =  new Headers({ 'Content-Type': 'application/json' });

  constructor(private http: Http, private localStorage : LocalStorageService) {}

  connect(action, parameters) {
    //todas las peticiones al servidor deben llevar el lenguage
    let lang = this.localStorage.getLang();
    parameters.language = (lang !== 'en') ? lang : 'e';

    let param = JSON.stringify({
      action: action,
      message: parameters
    });

    let options = new RequestOptions({ 
      headers: this.headers,
      body: param
    });

//******** Remote Data ******      
    console.log(options);
    return this.http.post(`${this.urlDataServer}`, options).map(res => res.json());
//*******************************

//******** Local Test Data ****** 

/*
    if(action === 'setCuestionarioSalud') {
      return this.http.get('assets/mock_getTreatmentAvailability.json').map(res => JSON.stringify(res.json()));
    } else if(action === 'getAppointments') {
      return this.http.get('assets/mock_getAppointments.json').map(res => JSON.stringify(res.json()));
    } else if(action === 'getCuestionarioSalud') {
      return this.http.get('assets/mock_getCuestionarioSalud.json').map(res => JSON.stringify(res.json()));
    } else if(action === 'getTreatmentAvailability') {
      return this.http.get('assets/mock_getTreatmentAvailability.json').map(res => JSON.stringify(res.json()));  
    } else if(action === 'getTreatmentsByCategory') {
      return this.http.get('assets/mock_getTreatmentsByCategory.json').map(res => JSON.stringify(res.json()));
    } else if(action === 'scheduleTreatment') {
      return this.http.get('assets/mock_getTreatmentsByCategory.json').map(res => JSON.stringify(res.json()));
    } else if(action === 'getBeautyTreatments') {
      return this.http.get('assets/mock_getBeautyTreatments.json').map(res => JSON.stringify(res.json()));
    } else if(action === 'getArrival') {
      return this.http.get('assets/mock_getArrival.json').map(res => JSON.stringify(res.json()));
    } else if(action === 'setArrival') {
      return this.http.get('assets/mock_getArrival.json').map(res => JSON.stringify(res.json()));
    } else if(action === 'getDeparture') {
      return this.http.get('assets/mock_getDeparture.json').map(res => JSON.stringify(res.json()));
    } else if(action === 'setDeparture') {
      return this.http.get('assets/mock_getDeparture.json').map(res => JSON.stringify(res.json()));
    } else if(action === 'getDoctor') {
      return this.http.get('assets/mock_getDoctor.json').map(res => JSON.stringify(res.json()));
    } else if(action === 'getShaAcademy') {
      return this.http.get('assets/mock_getShaAcademy.json').map(res => JSON.stringify(res.json()));  
    } else if(action === 'getNewsletter') {
      return this.http.get('assets/mock_getNewsletter.json').map(res => JSON.stringify(res.json()));  
    } else if(action === 'changePassword') {
      return this.http.get('assets/mock_changePassword.json').map(res => JSON.stringify(res.json()));  
    } else if(action === 'getTransferPrice') {
      return this.http.get('assets/mock_getTransferPrice.json').map(res => JSON.stringify(res.json()));  
    } else if(action === 'requestInvoice') {
      return this.http.get('assets/mock_getRequestInvoice.json').map(res => JSON.stringify(res.json()));  
    } else if(action === 'getSurvey') {
      return this.http.get('assets/mock_getSurvey.json').map(res => JSON.stringify(res.json()));  
    } else if(action === 'setSurvey') {
      return this.http.get('assets/mock_setSurvey.json').map(res => JSON.stringify(res.json()));  
    } else if(action === 'setContactRequest') {
      return this.http.get('assets/mock_setContactRequest.json').map(res => JSON.stringify(res.json()));  
    } else {   
      return this.http.get('assets/mock_getUser.json').map(res => JSON.stringify(res.json()));
    }
*/
//*******************************
  }


}
