import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TranslateService } from 'ng2-translate';
import { RemoteStorageService } from '../providers/remote-storage-service';


@Injectable()
export class DoctorData {

  constructor(private remoteStorage: RemoteStorageService, private translateService: TranslateService) {}

  getDoctor(id, role) {
    return new Observable(observer => {
      console.log(id);
      let params = {
        id: id,
        role: role
      };
      this.remoteStorage.connect('getDoctor', params).subscribe(
        (dt) => {
          let data = JSON.parse(dt);
          if (data.result == 1) {
            observer.next(data.message);
            observer.complete();
          } else {
            this.translateService.get('ERRORES').subscribe((val) => {
              observer.error(val.SERVIDOR);
            });
          }
        },
        (error) => {
          this.translateService.get('ERRORES').subscribe((val) => {
            observer.error(val.CONEXION_SERVIDOR);
          });
        }
      )
    });
  }

}
